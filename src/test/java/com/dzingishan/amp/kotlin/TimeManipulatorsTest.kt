package com.dzingishan.amp.kotlin

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.runners.MockitoJUnitRunner
import kotlin.test.assertFails

@RunWith(MockitoJUnitRunner::class)
class TimeManipulatorsTest {
    var timeManipulator = TimeManipulators()

    @Test
    fun testMillisToStaticTime() {
        assertEquals(TimeManipulators.fromMillisToStatic(345600), 0)
        assertEquals(TimeManipulators.fromMillisToStatic(1507248000), 345600)
        assertEquals(TimeManipulators.fromMillisToStatic(1507238625), 336225)
    }


    @Test
    fun testTextToStaticTime() {
        assertEquals(TimeManipulators.parseStaticTimer("10:00_Mon"), 36000)
        assertEquals(TimeManipulators.parseStaticTimer("12:00_Tue"), 129600)
        assertEquals(TimeManipulators.parseStaticTimer("12:23_Wed"), 217380)
        assertEquals(TimeManipulators.parseStaticTimer("11:32_Thu"), 300720)
        assertEquals(TimeManipulators.parseStaticTimer("23:43_Fri"), 430980)
        assertEquals(TimeManipulators.parseStaticTimer("09:00_Sat"), 464400)
        assertEquals(TimeManipulators.parseStaticTimer("01:00_Sun"), 522000)

        assertFails { TimeManipulators.parseStaticTimer("INVALID_TIME") }
        assertFails { TimeManipulators.parseStaticTimer("10:10_ASD") }
    }

    @Test
    fun testStaticToTest() {
        assertEquals(TimeManipulators.toReadableFromStatic(TimeManipulators.parseStaticTimer("10:00_mon")), "Mon_10:00:00")
    }
}