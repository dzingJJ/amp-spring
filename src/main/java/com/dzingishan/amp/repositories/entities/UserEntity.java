package com.dzingishan.amp.repositories.entities;

import com.dzingishan.amp.repositories.entities.jsons.SetGroupSerializer;
import com.dzingishan.amp.repositories.entities.jsons.UserDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.tritonus.share.ArraySet;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@JsonDeserialize(using = UserDeserializer.class)
@Table(name = "users")
public class UserEntity {
    @Id
    public String username;
    public String name;
    public String surname;
    public String additionalNameData;

    @ManyToMany(fetch = FetchType.EAGER, targetEntity = GroupEntity.class)
    @JsonSerialize(using = SetGroupSerializer.class)
    public Set<GroupEntity> userGroups = new ArraySet<>();
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> permissions;

    @JsonIgnore
    public String passKey;
    public String email;

    public final Set<String> getAllPermissions() {
        Set<String> list = new HashSet<>(this.permissions);
        this.userGroups.forEach(it -> it.getPermissions().stream().filter(i -> !list.contains(i)).forEach(list::add));
        return list;
    }

    public UserEntity() {
    }

    public UserEntity(String username, String name, String surname, String additionalNameData, Set<GroupEntity> userGroups, List<String> permissions, String passKey, String email) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.additionalNameData = additionalNameData;
        this.userGroups = userGroups;
        this.permissions = permissions;
        this.passKey = passKey;
        this.email = email;
    }

    public final boolean hasPermission(String s) {
        if (this.permissions.contains(s) || this.permissions.contains("*"))
            return true;
        String[] split = s.split("\\.");
        StringBuilder builder = new StringBuilder(split[0]);
        if (this.permissions.contains(builder + ".*"))
            return true;
        for (int i = 1; i < split.length; i++) {
            builder.append(".").append(split[i]);
            if (this.permissions.contains(builder + ".*"))
                return true;
        }
        return this.userGroups.stream().anyMatch(it -> it.containsPermission(s));
    }

}