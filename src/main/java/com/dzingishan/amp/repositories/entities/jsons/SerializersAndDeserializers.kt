package com.dzingishan.amp.repositories.entities.jsons

import com.dzingishan.amp.ProcessingException
import com.dzingishan.amp.kotlin.Constants
import com.dzingishan.amp.repositories.entities.GroupEntity
import com.dzingishan.amp.repositories.entities.PlaylistEntry
import com.dzingishan.amp.repositories.entities.UserEntity
import com.dzingishan.amp.repositories.entities.YoutubeEntry
import com.dzingishan.amp.services.GroupAndUsersService
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCrypt
import java.io.IOException

class SetGroupSerializer : JsonSerializer<Set<GroupEntity>>() {
    override fun serialize(p0: Set<GroupEntity>, p1: JsonGenerator, p2: SerializerProvider?) {
        p1.writeStartArray()
        p0.forEach { p1.writeString(it.groupName) }
        p1.writeEndArray()
    }


}

class UserDeserializer : JsonDeserializer<UserEntity>() {

    @Autowired
    lateinit var groupService: GroupAndUsersService

    @Throws(IOException::class)
    override fun deserialize(jsonParser: JsonParser, deserializationContext: DeserializationContext): UserEntity {
        val node = jsonParser.codec.readTree<JsonNode>(jsonParser)
        if (!node.has("username") || !node.has("name") || !node.has("surname") || !node.has("email")) {
            throw ProcessingException(("invalid_user"))
        }
        if (!node.has("passKey")) {
            throw ProcessingException(("no_password_supplied"))
        }
        val u = UserEntity()
        u.name = node.get("name").asText()
        u.username = node.get("username").asText()
        u.surname = node.get("surname").asText()
        if (u.name.equals("console", true) || u.username.equals("console", true) || u.username.equals("console", true) || u.username.equals("file_loaded", true)) {
            throw ProcessingException("Invalid request")
        }
        //u.passKey = String(CryptographyUtils.decrypt(node.get("passKey").asText().toByteArray(Charsets.UTF_8)), Charsets.UTF_8) TODO
        u.passKey = BCrypt.hashpw(node.get("passKey").asText(), BCrypt.gensalt())
        u.email = node.get("email").asText()

        if (node.has("additionalNameData"))
            u.additionalNameData = node.get("additionalNameData").asText()

        u.userGroups.add(groupService.defaultGroup)
        return u
    }
}

class YoutubeEntrySerializer : JsonSerializer<YoutubeEntry>() {
    override fun serialize(yt: YoutubeEntry, json: JsonGenerator, p2: SerializerProvider?) {
        json.writeStartObject()
        json.writeObjectField("fullTitle", yt.fullTitle)
        json.writeObjectField("title", yt.title)
        json.writeObjectField("author", yt.author)
        json.writeObjectField("fileName", yt.fileName)
        json.writeObjectField("tags", yt.tags)
        json.writeObjectField("username", yt.user?.username ?: Constants.CONSOLE_USER)
        json.writeObjectField("url", yt.url)
        json.writeObjectField("duration", yt.duration)
        json.writeObjectField("id", yt.id)
        json.writeEndObject()
    }
}

open class PlaylistEntrySerializer : JsonSerializer<PlaylistEntry>() {
    override fun serialize(p0: PlaylistEntry, p1: JsonGenerator, p2: SerializerProvider?) {
        p1.writeStartObject()
        p1.writeObjectField("songName", p0.songEntity!!.title)
        p1.writeObjectField("scheduler", p0.scheduler!!.username)
        p1.writeObjectField("songId", p0.songEntity!!.numberId)
        p1.writeObjectField("scheduleTime", p0.scheduleTime)
        p1.writeObjectField("number", p0.number)
        p1.writeEndObject()
    }

}