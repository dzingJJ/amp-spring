package com.dzingishan.amp.repositories.entities

import com.dzingishan.amp.kotlin.Constants
import com.dzingishan.amp.kotlin.TimeManipulators
import com.dzingishan.amp.repositories.entities.jsons.PlaylistEntrySerializer
import com.dzingishan.amp.repositories.entities.jsons.YoutubeEntrySerializer
import com.dzingishan.amp.services.SettingsService
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import org.hibernate.annotations.LazyCollection
import org.hibernate.annotations.LazyCollectionOption
import java.io.File
import javax.persistence.*

@Entity
@Table(name = "groups")
open class GroupEntity(@Id var groupName: String = "", @ElementCollection @LazyCollection(LazyCollectionOption.FALSE) var permissions: Set<String> = mutableSetOf()) {

    fun containsPermission(s: String): Boolean {
        if (permissions.contains(s) || permissions.contains("*"))
            return true
        val split = s.split("\\.")
        val builder = StringBuilder(split[0])
        if (permissions.contains(builder.toString() + ".*"))
            return true
        (1 until split.size).forEach { i ->
            builder.append(".${split[i]}")
            if (permissions.contains(builder.toString() + ".*"))
                return true
        }
        return false
    }
}

@Entity
@Table(name = "day_entries")
class ProgramDayEntity(var createTime: Long = TimeManipulators.currentTimeWithTimeZoneSeconds(), var creator: String = "", var relativeStartTime: Int = -1, var relativeEndTime: Int = -1, @Id @GeneratedValue var entryId: Long = 0)

@Entity
@Table(name = "songs")
class SongEntity(var songName: String = "", var creator: String = "", var title: String = "", var artist: String = "", var additionalTags: String = "", @ElementCollection @LazyCollection(LazyCollectionOption.FALSE) var tags: Set<String> = mutableSetOf(), var duration: Int = 0, @Id @GeneratedValue var numberId: Int = 0, var fileName: String = "") {
    @Transient
    var file: File = File("a.a")
        get() = File(SettingsService.songsFolder, fileName)

    fun toSongDetails(): SongDetails = SongDetails(songName, title, artist, numberId, duration)

    override fun equals(other: Any?): Boolean {
        if (other !is SongEntity)
            return false
        return songName == other.songName && fileName == other.fileName
    }
}

@Entity
@Table(name = "special_permits")
class SpecialDayPermitEntity(var permitCreator: String = "", var permitHolder: String = "", var fromTime: Long = 0, var toTime: Long = 0) {

    @Id
    @GeneratedValue
    var permitId: Long = 0

}

@Entity
@Table(name = "playlist")
@JsonSerialize(using = PlaylistEntrySerializer::class)
class PlaylistEntry(
        @JoinColumn(name = "songIden") @ManyToOne var songEntity: SongEntity? = null,
        @JoinColumn(name = "userIdem") @ManyToOne var scheduler: UserEntity? = null,
        @Id var scheduleTime: Long = System.currentTimeMillis(),
        var number: Int = 0
)


@Entity
@Table(name = "special_schedules")
class SpecialScheduleEntity(@JoinColumn(name = "songIden") @ManyToOne var songEntity: SongEntity? = null, @JoinColumn(name = "userid") @ManyToOne var scheduler: UserEntity? = null, @Id var absoluteStartTime: Long = 0L, var ignoreWeekTimer: Boolean = false)

data class SongDetails(var name: String, var title: String, var artist: String, var id: Int, var duration: Int)

@Entity
@JsonSerialize(using = YoutubeEntrySerializer::class)
data class YoutubeEntry(val fullTitle: String = "", val title: String = "", val author: String = "", var fileName: String = "", @ElementCollection @LazyCollection(LazyCollectionOption.FALSE) val tags: MutableList<String>? = null, @JoinColumn(name = "userid") @ManyToOne var user: UserEntity? = null, var url: String = "", var error: Boolean = false, var duration: Int = 0, @Id @GeneratedValue var id: Int = 0) {
    val file: File
        get() = File(SettingsService.acceptFolder, fileName)

    fun toSongEntity(): SongEntity {
        return SongEntity(
                songName = fullTitle,
                creator = user?.username ?: Constants.CONSOLE_USER,
                title = title,
                artist = author,
                additionalTags = url,
                tags = tags?.toSet() ?: setOf(),
                duration = duration,
                fileName = fileName
        )
    }
}

@Entity
data class VolumeEntry(@Id @GeneratedValue var volumeId: Int = 0, var staticTimeStart: Int = -2, var staticTimeEnd: Int = -3, var volume: Double = 0.0)