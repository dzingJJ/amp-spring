package com.dzingishan.amp.repositories

import com.dzingishan.amp.repositories.entities.*
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional

interface SongRepository : JpaRepository<SongEntity, Int> {

    @Query("SELECT s FROM SongEntity s WHERE s.numberId >= RAND() * (SELECT MAX(numberId) FROM SongEntity)")
    fun getRandomSong(): List<SongEntity>

    fun findByTitleContaining(title: String, page: Pageable): Page<SongEntity>

    @Query("SELECT count(e)>0 FROM SongEntity e WHERE UPPER(e.title) = UPPER(:title)")
    fun existsByTitle(@Param("title") title: String): Boolean

    fun findOneByFileName(fileName: String): SongEntity?

    @Query("SELECT s FROM SongEntity s WHERE UPPER(s.title) LIKE UPPER(:title) and UPPER(s.artist) LIKE UPPER(:author)")
    fun findOneByAuthorTitle(@Param("title") title: String, @Param("author") author: String): SongEntity?

    @Query("SELECT s FROM SongEntity s WHERE s.additionalTags = :url")
    fun findOneByUrl(@Param("url") url: String): SongEntity?

}

interface SpecialPermitsRepository : JpaRepository<SpecialDayPermitEntity, Long> {

    @Query("SELECT e FROM SpecialDayPermitEntity e where e.fromTime <= :time and e.toTime >= :time")
    fun findMatchingTime(@Param("time") time: Long): SpecialDayPermitEntity?

    @Query("DELETE FROM SpecialDayPermitEntity s WHERE s.fromTime < :currentTime and s.toTime < :currentTime")
    @Modifying
    @Transactional
    fun clearOld(@Param("currentTime") currentTime: Long)
}

interface UserRepository : JpaRepository<UserEntity, String> {
    fun existsByEmail(email: String): Boolean

    fun findOneByUsername(username: String): UserEntity?
}

interface ProgramDaysRepository : JpaRepository<ProgramDayEntity, Long> {
    @Query("SELECT e FROM ProgramDayEntity e where e.relativeStartTime <= :statictime and e.relativeEndTime >= :statictime")
    fun findOneMatchingTime(@Param("statictime") staticTime: Int): ProgramDayEntity?

}

interface GroupRepository : JpaRepository<GroupEntity, String> {

    fun existsByGroupName(groupName: String): Boolean

    fun findByGroupName(groupName: String): GroupEntity

}

interface PlaylistRepository : JpaRepository<PlaylistEntry, Long> {
    @Query("SELECT e FROM PlaylistEntry e ORDER BY number")
    fun findOneNext(): List<PlaylistEntry>

    @Query("SELECT e FROM PlaylistEntry e WHERE e.number >= :startPos")
    fun findAllWithBiggerNumber(@Param("startPos") startPos: Int): List<PlaylistEntry>

    @Query("SELECT e FROM PlaylistEntry e ORDER BY number DESC")
    fun findOneLast(): List<PlaylistEntry>

    fun findOneByNumber(number: Int): PlaylistEntry?
}

interface SpecialScheduleRepository : JpaRepository<SpecialScheduleEntity, Long> {
    @Query("SELECT e FROM SpecialScheduleEntity e where e.absoluteStartTime <= :time ORDER BY absoluteStartTime DESC")
    fun findMatchingTime(@Param("time") time: Long): SpecialScheduleEntity?

    @Query("DELETE FROM SpecialScheduleEntity WHERE absoluteStartTime < :currentTime")
    @Modifying
    @Transactional
    fun clearOld(@Param("currentTime") currentTime: Long)
}

interface YoutubeEntryRepository : JpaRepository<YoutubeEntry, Int> {
    fun existsByFullTitle(fullTitle: String): Boolean
    fun findOneByFileName(name: String): YoutubeEntry?
    fun findOneByUrl(songYTUrl: String): YoutubeEntry?
}

interface VolumeEntryRepository : JpaRepository<VolumeEntry, Int> {
    @Query("SELECT e FROM VolumeEntry e WHERE e.staticTimeStart <= :time AND e.staticTimeEnd >= :time")
    fun findOneMatchingTime(@Param("time") time: Int): List<VolumeEntry?>
}


fun PlaylistRepository.decrementAllByOne() {
    val list = findAll()
    list.forEach { it.number-- }
    save(list)
    flush()
}

fun PlaylistRepository.incrementAllFrom(startPos: Int) {
    val list = findAllWithBiggerNumber(startPos)
    list.forEach { it.number++ }
    save(list)
    flush()
}

fun PlaylistRepository.decrementAllFrom(startPos: Int) {
    val list = findAllWithBiggerNumber(startPos)
    list.forEach { it.number-- }
    save(list)
    flush()
}