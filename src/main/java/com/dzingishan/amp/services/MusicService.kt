package com.dzingishan.amp.services

import com.dzingishan.amp.ProcessingException
import com.dzingishan.amp.events.MusicPlayEvent
import com.dzingishan.amp.events.MusicStopEvent
import com.dzingishan.amp.events.TickEvent
import com.dzingishan.amp.kotlin.TimeManipulators
import com.dzingishan.amp.repositories.*
import com.dzingishan.amp.repositories.entities.PlaylistEntry
import com.dzingishan.amp.repositories.entities.SongEntity
import com.dzingishan.amp.repositories.entities.UserEntity
import com.dzingishan.amp.repositories.entities.VolumeEntry
import com.dzingishan.amp.security.KeyAuthentication
import javazoom.jlgui.basicplayer.BasicController
import javazoom.jlgui.basicplayer.BasicPlayer
import javazoom.jlgui.basicplayer.BasicPlayerEvent
import javazoom.jlgui.basicplayer.BasicPlayerListener
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct


@Service
class MusicService {

    /**
     * TODO: Add smth like "random song with tag"
     */

    //Logger
    private var logger = Logger.getLogger(MusicService::class.qualifiedName)

    //Autowired
    //Autowired fields
    @Autowired private lateinit var noRepeatPlaylist: List<String>
    @Autowired private lateinit var basicPlayer: BasicPlayer
    @Autowired private lateinit var publisher: ApplicationEventPublisher

    //Autowired Repositories
    @Autowired private lateinit var programDaysRepository: ProgramDaysRepository
    @Autowired private lateinit var settingsService: SettingsService
    @Autowired private lateinit var songRepo: SongRepository
    @Autowired private lateinit var specialScheduleRepository: SpecialScheduleRepository
    @Autowired private lateinit var specialPermitRepository: SpecialPermitsRepository
    @Autowired private lateinit var playlist: PlaylistRepository
    @Autowired lateinit var volumeRepository: VolumeEntryRepository //TODO: Private

    //Autowired services
    @Autowired private lateinit var songService: SongService
    @Autowired private lateinit var externalResourcesService: ExternalResourcesService

    //Volume connected variables
    private var _backingVol = 0.1
    private var volumeValue = 100.0
    private var fadingOut = false
    private var tempVolume = 0.toDouble()
    private var lastTickTime: Long = 0
    private var lastVolume: Double = 0.0
    private var currentVolume: VolumeEntry
        get() {
            var entry = volumeRepository.findOneMatchingTime(TimeManipulators.currentStaticTime())
            val e: VolumeEntry?
            if (entry.isEmpty()) {
                entry = volumeRepository.findOneMatchingTime(-1)
                e = if (entry.isEmpty()) {
                    logger.fatal("ERROR: Entry is null!")
                    VolumeEntry(0, 0, 0, 50.0)
                } else {
                    entry[0]!!
                }
            } else {
                e = entry[0]!!
            }
            return e
        }
        set(value) = Unit
    var volume: Double
        get() = (_backingVol * 500) + currentVolume.volume
        set(value) {
            volumeValue = value
            _backingVol = (value - currentVolume.volume) / 500
            if (_backingVol < 0)
                _backingVol = 0.005
            else if (_backingVol > 100)
                _backingVol = 100.0
            logger.trace("Setting volume to $_backingVol")
            if (isPlaying)
                basicPlayer.setGain(_backingVol)
        }

    //Player connected variables
    var isPlaying = false
    var secondPassed = 0
    var currentlyPlaying: ScheduledSong? = null
    var lastSong: ScheduledSong? = null

    private var ignoringTime = false


    //Functions
    //Spring Annotated functions
    @PostConstruct
    fun startClient() {
        logger.debug("Starting Music Service")
        basicPlayer.addBasicPlayerListener(BasicPlayerListenerImpl())
        if (volumeRepository.findOneMatchingTime(-1).isEmpty()) {
            volumeRepository.saveAndFlush(VolumeEntry(staticTimeStart = -1, staticTimeEnd = -1, volume = 0.0))
        }
    }

    @PostConstruct
    fun cleanTask() {
        logger.debug("Cleaning all leftovers of old schedules")
        val currentTime = TimeManipulators.currentTimeWithTimeZoneSeconds()
        specialScheduleRepository.clearOld(currentTime)
        specialPermitRepository.clearOld(currentTime)
    }

    @Scheduled(fixedRate = 60000)
    private fun checkTheQueue() {
        logger.debug("Checking the queue")
        var a = 1
        playlist.save<PlaylistEntry?>(playlist.findAll().sortedBy { it.number }.map { it -> it.apply { it.number = a++ } })
        playlist.flush()
        logger.debug("Queue checked!")
    }

    @Scheduled(initialDelay = 100, fixedRate = 500)
    fun tick() {
        logger.trace("Music service tick")
        if (SettingsService.dummyMode && basicPlayer.hasGainControl()) {
            basicPlayer.setGain(0.0)
        }
        if (SettingsService.globalLock || SettingsService.stop) {
            logger.debug("Global Lock Enabled!")
            if (isPlaying)
                basicPlayer.stop()
            return
        }
        val tick = TickEvent()
        publisher.publishEvent(tick)
        if (!tick.continueOperation)
            return
        val currentTime = TimeManipulators.currentTimeWithTimeZoneSeconds() + SettingsService.secondsDelay
        if (!isPlaying) {
            val perm = (specialScheduleRepository.findMatchingTime(currentTime))
            if (perm != null) {
                logger.debug("User with nickname ${perm.scheduler?.username} is using permission to play song ${perm.songEntity?.songName}")
                specialScheduleRepository.delete(perm)
                ignoringTime = perm.ignoreWeekTimer
                play(perm.songEntity!!, perm.scheduler!!.username)
            } else if (canPlayWeek(currentTime) && timeLeft(currentTime) > 30) {
                val playlistEntry = removeGetDecrement()
                if (playlistEntry != null) {
                    logger.debug("Playing song ${playlistEntry.songEntity?.songName}")
                    play(playlistEntry.songEntity!!, playlistEntry.scheduler!!.username)
                } else if (SettingsService.randomPlaying && timeLeft(currentTime) > 30) {
                    logger.debug("Playing random song!")
                    play(getAndAddRandom(), "RANDOM")
                }
            }
        } else {
            if (fadingOut) {
                fadeTick()
            } else if (!ignoringTime && !SettingsService.globalIgnore && !canPlayWeek(currentTime)) {
                fadeOutMusic()
            } else {
                volume = volumeValue
            }
        }

    }

    //Private functions
    private fun play(song: SongEntity, scheduler: String) {
        logger.info("${song.title} by ${song.artist}, added by $scheduler is now playing with volume $_backingVol!")
        currentlyPlaying = ScheduledSong(song, scheduler)
        basicPlayer.open(song.file)
        basicPlayer.play()
        basicPlayer.setGain(_backingVol)
        if (SettingsService.dummyMode) {
            basicPlayer.setGain(0.0)
        }
    }

    private fun getNextSong(): PlaylistEntry? = playlist.findOneNext().getOrNull(0)

    private fun removeGetDecrement(): PlaylistEntry? {
        val pl = getNextSong() ?: return null
        playlist.delete(pl)
        playlist.decrementAllByOne()
        return pl
    }

    private fun fadeTick() {
        if (System.currentTimeMillis() - lastTickTime > 300) {
            lastTickTime = System.currentTimeMillis()
            logger.debug("Ticking to ${volume - 5}")
            if (lastVolume != volume) {
                volume -= 5
                lastVolume = volume
            } else {
                logger.debug("Fading complete! Stopping music.")
                basicPlayer.stop()
                volume = tempVolume
                tempVolume = 0.0
                lastVolume = 0.0
            }
        }
        if (SettingsService.dummyMode) {
            basicPlayer.setGain(0.0)
        }
    }

    private fun canPlayWeek(staticTime: Long): Boolean =
            programDaysRepository.findOneMatchingTime(TimeManipulators.fromMillisToStatic(staticTime)) != null

    private fun timeLeft(millisTime: Long): Int {
        val entry = programDaysRepository.findOneMatchingTime(TimeManipulators.fromMillisToStatic(millisTime))
        return if (entry == null) {
            0
        } else {
            entry.relativeEndTime - TimeManipulators.fromMillisToStatic(millisTime)
        }
    }

    private fun removeSong(playlistEntry: PlaylistEntry) {
        playlist.delete(playlistEntry)
        playlist.decrementAllFrom(playlistEntry.number)
    }

    //Public classes
    fun stop() {
        logger.debug("Stopping music!")
        basicPlayer.stop()
    }

    fun getAndAddRandom(): SongEntity {
        var b = 0
        do {
            return songRepo.getRandomSong().firstOrNull { !noRepeatPlaylist.contains(it.songName) }?.also { noRepeatPlaylist += it.songName } ?: continue
        } while (b++ > 10)
        noRepeatPlaylist.drop(noRepeatPlaylist.size)
        return songRepo.getRandomSong().first().also { noRepeatPlaylist += it.songName }
    }

    fun removeSong(songNumber: Int): Boolean {
        removeSong(playlist.findOneByNumber(songNumber) ?: return false)
        return true
    }

    fun insertSong(songNumber: Int, target: Int, userEntity: UserEntity): Boolean {
        if (target < 1) return false
        var targetNumber = target
        val last = playlist.findOneLast().getOrNull(0)?.number ?: 1
        if (targetNumber > last)
            targetNumber = last
        println("$songNumber $target $targetNumber")
        val playlistEntry = PlaylistEntry(songRepo.findOne(songNumber) ?: return false, userEntity, number = targetNumber)
        playlist.incrementAllFrom(target)
        playlist.saveAndFlush(playlistEntry)
        return true
    }

    fun scheduleSong(songId: Int, u: UserEntity? = null): Boolean {
        val song = songService.getSong(songId) ?: return false
        scheduleSong(song, u)
        return true
    }

    private fun scheduleSong(song: SongEntity, u: UserEntity? = null) {
        val user = ((SecurityContextHolder.getContext()?.authentication as KeyAuthentication?)?.user ?: u) ?: throw ProcessingException("Error: User is NULL When no SecurityContext Defined!")
        val id = playlist.findOneLast().getOrNull(0)?.number?.plus(1) ?: 1
        val entry = PlaylistEntry(song, user, number = id)
        playlist.saveAndFlush(entry)
    }

    fun songDeleting(songEntity: SongEntity) {
        playlist.delete(playlist.findAll().filter { it.songEntity!! == songEntity })
        if (currentlyPlaying != null) {
            if (currentlyPlaying!!.song == songEntity) {
                stop()
            }
        }
    }

    fun fadeOutMusic() {
        logger.debug("Starting fading out music")
        volume -= 1
        tempVolume = volume
        fadingOut = true
        lastTickTime = System.currentTimeMillis()
    }

    fun swapSongs(song1: Int, song2: Int): Boolean {
        val songPl1 = playlist.findOneByNumber(song1) ?: return false
        val songPl2 = playlist.findOneByNumber(song2) ?: return false
        val temp = songPl1.number
        songPl1.number = songPl2.number
        songPl2.number = temp
        playlist.saveAndFlush(songPl1)
        playlist.saveAndFlush(songPl2)
        return true
    }

    fun planFromPlaylist(url: String, user: UserEntity): Boolean {
        logger.debug("Scheduling song")
        if (externalResourcesService.isSpotifyLink(url)) {
            logger.debug("Detected spotify playlisr!")
            val songs = externalResourcesService.getSpotifyPlaylist(url)
            songs.map {
                logger.info("Adding " + it.title + " " + it.artist + " To the playlist")
                externalResourcesService.getInAnyMethodSpotify(it.title, it.artist, user)
            }.forEach { scheduleSong(it, user) }
            return true
        } else if (externalResourcesService.isYoutubePlaylistLink(url)) {
            logger.debug("Detected youtube playlist")
            externalResourcesService.getYoutubePlaylistUrls(externalResourcesService.extractPlaylistLink(url)).mapNotNull { externalResourcesService.getInAnyMethodYoutube(url, user) }.forEach { scheduleSong(it, user) }
            return true
        }
        logger.debug("Not supported playlist type detected!")
        return false
    }


    //Inner classes
    inner class BasicPlayerListenerImpl : BasicPlayerListener {

        override fun opened(stream: Any, properties: Map<*, *>) {
        }

        override fun progress(bytesread: Int, microseconds: Long, pcmdata: ByteArray, properties: Map<*, *>) {
            secondPassed = (microseconds / 1000000).toInt()
            if (SettingsService.dummyMode) {
                basicPlayer.setGain(0.0)
            }
        }

        override fun stateUpdated(bpe: BasicPlayerEvent) {
            if (SettingsService.dummyMode) {
                basicPlayer.setGain(0.0)
            }
            if (bpe.code == BasicPlayerEvent.PLAYING) {
                if (currentlyPlaying == null && lastSong != null) {
                    currentlyPlaying = lastSong
                    secondPassed = 0
                    isPlaying = true
                    publisher.publishEvent(MusicPlayEvent(currentlyPlaying!!))
                    logger.warn("Issue with current song in song ${lastSong?.song?.songName}: Starting playing with currently playing == null")
                } else if (currentlyPlaying == null && lastSong == null) {
                    logger.error("Playing song from external source!")
                } else {
                    secondPassed = 0
                    isPlaying = true
                }
            }

            if (bpe.code == BasicPlayerEvent.STOPPED) {
                logger.info("End of song ${currentlyPlaying?.song?.title}")
                if (currentlyPlaying != null) {
                    publisher.publishEvent(MusicStopEvent(currentlyPlaying!!))
                    lastSong = currentlyPlaying
                    currentlyPlaying = null
                }
                secondPassed = 0
                isPlaying = false
                ignoringTime = false
                fadingOut = false
            }
        }

        override fun setController(bc: BasicController) {
        }
    }

}

data class ScheduledSong(val song: SongEntity, val scheduler: String)