/*
 * Copyright (c) 2017. Grzegorz Dzikowski
 * All Rights Reserved.
 * Any modification, copying, redistribution is strictly forbidden
 */

package com.dzingishan.amp.services

import com.dzingishan.amp.spring.AMP
import org.apache.commons.io.FileUtils
import org.apache.log4j.Logger
import org.springframework.stereotype.Service
import java.io.File
import java.io.IOException
import java.lang.RuntimeException
import java.util.*
import javax.annotation.PostConstruct


@Service
open class LibrariesService {

    var logger = Logger.getLogger(LibrariesService::class.qualifiedName)

    companion object {
        val LIBS: File = File("libs")

        var loader = AMP::class.java.classLoader

        var YOUTUBE_DL_PATH = "youtube-dl"
        var FFMPEG_PATH = ""
        var MP3GAIN_PATH = "mp3gain"
        var NORMALIZE_PATH = "normalize"
    }

    @PostConstruct
    fun loadLibraries() {
        val system = OsCheck.detectedOS
        if (system == OsCheck.OSType.Other)
            throw (RuntimeException("Unknown system: " + System.getProperty("os.name")))
        else if (system == OsCheck.OSType.MacOS)
            throw (RuntimeException("MacOS is currently not supported :("))

        val arch = System.getProperty("os.arch")
        var is64 = when (arch) { //TODO
            "x86", "i386" -> false
            "amd64", "x86_64" -> true
            else -> throw (RuntimeException("Unknown archtype: $arch "))
        }

        logger.warn("Os is " + system)
        when (system) {
            OsCheck.OSType.Windows -> {
                if (!checkIfItIsInstalled("youtube-dl", name = "YoutubeDL")) {
                    val ytPath = File("libs/youtube-dl.exe")
                    if (ytPath.exists()) {
                        YOUTUBE_DL_PATH = ytPath.absolutePath
                        logger.info("Youtube-dl found in libs!")
                        SettingsService.youtubeDownloadWorks = true
                    } else {
                        logger.warn("YOUTUBE-DL NOT FOUND! DISABLING YOUTUBE DOWNLOADING FEATURE!")
                        SettingsService.youtubeDownloadWorks = false
                    }
                } else {
                    SettingsService.youtubeDownloadWorks = true
                }

                if (!checkIfItIsInstalled("ffmpeg", name = "FFMPEG") || !checkIfItIsInstalled("ffprobe", name = "FFPROBE")) {
                    val ffmpegPath = File("libs/ffmpeg.exe")
                    val ffprobePath = File("libs/ffprobe.exe")
                    if (ffmpegPath.exists() && ffprobePath.exists()) {
                        FFMPEG_PATH = ffmpegPath.parent
                        logger.info("FFMpeg and FFProbe found in libs!")
                        SettingsService.youtubeDownloadWorks = true
                    } else {
                        logger.warn("FFMPEG AND FFPROBE NOT FOUND! DISABLING YOUTUBE DOWNLOADING FEATURE!")
                        SettingsService.youtubeDownloadWorks = false
                    }
                } else {
                    SettingsService.youtubeDownloadWorks = true
                }

                if (!checkIfItIsInstalled("mp3gain", name = "MP3GAIN")) {
                    val mp3GainPath = File("libs/mp3gain.exe")
                    if (mp3GainPath.exists()) {
                        MP3GAIN_PATH = mp3GainPath.absolutePath
                        logger.info("MP3Gain found in libs!")
                        SettingsService.mp3GainWorks = true
                    } else {
                        logger.warn("MP3GAIN NOT FOUND! DISABLING MP3 NORMALIZING!")
                        SettingsService.mp3GainWorks = false
                    }
                } else {
                    SettingsService.mp3GainWorks = true
                }

                if (!checkIfItIsInstalled("normalize", name = "NORMALIZE")) {
                    val normalizePath = File("libs/normalize.exe")
                    if (normalizePath.exists()) {
                        NORMALIZE_PATH = normalizePath.absolutePath
                        logger.info("Normalize found in libs!")
                        SettingsService.normalizerWorks = true
                    } else {
                        logger.warn("NORMALIZE NOT FOUND! DISABLING WAV NORMALIZING!")
                        SettingsService.normalizerWorks = false
                    }
                } else {
                    SettingsService.normalizerWorks = true
                }

            }
            OsCheck.OSType.Linux -> {
                logger
                if (!checkIfItIsInstalled("youtube-dl", name = "YoutubeDL")) {
                    logger.warn("YOUTUBE-DL NOT FOUND! DISABLING YOUTUBE DOWNLOADING FEATURE!")
                    SettingsService.youtubeDownloadWorks = false
                } else {
                    SettingsService.youtubeDownloadWorks = true
                }

                if (!checkIfItIsInstalled("avconv", name = "AvConv") || !checkIfItIsInstalled("avprobe", name = "AvProbe")) {
                    logger.warn("AvConv AND AvProbe NOT FOUND! DISABLING YOUTUBE DOWNLOADING FEATURE!")
                    SettingsService.youtubeDownloadWorks = false
                } else {
                    SettingsService.youtubeDownloadWorks = true
                }

                if (!checkIfItIsInstalled("mp3gain", name = "MP3GAIN")) {
                    val mp3GainPath = File("libs/mp3gain")
                    if (mp3GainPath.exists()) {
                        MP3GAIN_PATH = mp3GainPath.absolutePath
                        logger.info("MP3Gain found in libs!")
                        SettingsService.mp3GainWorks = true
                    } else {
                        logger.warn("MP3GAIN NOT FOUND! DISABLING MP3 NORMALIZING!")
                        SettingsService.mp3GainWorks = false
                    }
                } else {
                    SettingsService.mp3GainWorks = true
                }

                if (!checkIfItIsInstalled("normalize-audio", name = "NORMALIZE")) {
                    logger.warn("NORMALIZE NOT FOUND! DISABLING WAV NORMALIZING!")
                    SettingsService.normalizerWorks = false
                } else {
                    NORMALIZE_PATH = "normalize-audio"
                    SettingsService.normalizerWorks = true
                }

            }
            else -> {
                throw RuntimeException("Unknown system: " + System.getProperty("os.name"))
            }
        }

    }


    private fun unpackLibs(path: String, filename: String): File {
        val file = File(LIBS, filename)
        FileUtils.copyInputStreamToFile(loader.getResourceAsStream(path), file)
        return file
    }

    private fun checkIfItIsInstalled(vararg strings: String, name: String = ""): Boolean {
        try {
            val b = ProcessBuilder(strings.asList())
            b.redirectInput(ProcessBuilder.Redirect.PIPE)
            val p = b.start()
            p.waitFor()
            if (name != "")
                logger.info("$name is installed!")
            return true
        } catch (ex: IOException) {
            if (name != "")
                logger.info("$name is not installed!")
            return false
        } catch (ex: InterruptedException) {
            if (name != "")
                logger.info("$name is not installed!")
            return false
        }
    }
}

/**
 * helper class to check the operating system this Java VM runs in
 * <p>
 * please keep the notes below as a pseudo-license
 * <p>
 * http://stackoverflow.com/questions/228477/how-do-i-programmatically-determine-operating-system-in-java
 * compare to http://svn.terracotta.org/svn/tc/dso/tags/2.6.4/code/base/common/src/com/tc/util/runtime/Os.java
 * http://www.docjar.com/html/api/org/apache/commons/lang/SystemUtils.java.html
 */


object OsCheck {
    private val OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH)
    val detectedOS =
            if (OS.indexOf("mac") >= 0 || OS.indexOf("darwin") >= 0) {
                OSType.MacOS
            } else if (OS.indexOf("win") >= 0) {
                OSType.Windows
            } else if (OS.indexOf("nux") >= 0) {
                OSType.Linux
            } else {
                OSType.Other
            }


    enum class OSType {
        Windows, MacOS, Linux, Other
    }
}