package com.dzingishan.amp.services

import com.dzingishan.amp.kotlin.Constants
import com.dzingishan.amp.musicanalysis.normalize
import com.dzingishan.amp.repositories.SongRepository
import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.YoutubeEntryRepository
import com.dzingishan.amp.repositories.entities.SongEntity
import com.dzingishan.amp.repositories.entities.YoutubeEntry
import org.apache.log4j.Logger
import org.jaudiotagger.audio.AudioFileIO
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.io.File
import java.nio.file.Files
import javax.annotation.PostConstruct

@Service
open class SongService {

    var logger = Logger.getLogger(SongService::class.qualifiedName)

    @Autowired lateinit var songsRepo: SongRepository
    @Autowired lateinit var userRepo: UserRepository
    @Autowired lateinit var youtubeRepo: YoutubeEntryRepository

    @Autowired lateinit var settingsService: SettingsService
    @Autowired lateinit var musicService: MusicService

    @PostConstruct
    private fun start() { //TODO: Accept clean list
        logger.debug("Starting SongService")
        rescanAll()
    }

    fun rescanAll() {
        logger.info("Rescanning songs")
        rescanForSongs()
        rescanForAcceptSongs()
    }

    private fun rescanForAcceptSongs() {
        logger.trace("Rescaning accept songs")
        SettingsService.acceptFolder.mkdirs()
        val list = youtubeRepo.findAll()
        list.toTypedArray().forEach {
            val file = File(SettingsService.acceptFolder, it.fileName)
            if (!file.exists()) {
                logger.warn("${it.fileName} not found! Deleting..")
                youtubeRepo.delete(it)
                list.remove(it)
            }
        }
        SettingsService.acceptFolder.listFiles({ a -> !list.any { a == it.file } }).forEach { tryToLoadAccept(f = it) }
        logger.info("Done songs scanning! Songs in db: ${songsRepo.count()}")
    }

    private fun tryToLoadAccept(f: File) {
        val file = File(f.parentFile, f.name.replace(Regex("[^\\p{ASCII}]"), ""))
        f.renameTo(file)
        logger.info("Trying to load ${file.name}")
        if (file.extension != "mp3" && file.extension != "wav") {
            logger.error(file.extension + " is not!")
            return
        }
        val duration = getDurationOfSong(f).toInt()
        try {
            logger.info("New song name " + file.name)
            val entity = YoutubeEntry(fullTitle = file.nameWithoutExtension, author = Constants.LOADED_FILE_NAME, duration = duration, fileName = file.name)
            normalize(entity.file)
            youtubeRepo.saveAndFlush(entity)
            logger.info("Successfully loaded {${file.name}!")
        } catch (e: Exception) {
            logger.error("An exception occurred while processing file $file.")
            e.printStackTrace()
        }
    }

    private fun tryToLoad(f: File) {
        val file = File(f.parentFile, f.name.replace(Regex("[^\\p{ASCII}]"), ""))
        f.renameTo(file)
        logger.info("Trying to load ${file.name}")
        if (file.extension != "mp3" && file.extension != "wav") {
            logger.error(file.extension + " is not!")
            return
        }
        val duration = getDurationOfSong(f).toInt()
        try {
            logger.info("New song name " + file.name)
            val split = file.nameWithoutExtension.split("_")
            val title = split[0]
            val author = split[1]
            val entity = SongEntity(songName = file.nameWithoutExtension, creator = Constants.LOADED_FILE_NAME, artist = author, title = title, duration = duration, fileName = file.name)
            normalize(entity.file)
            songsRepo.saveAndFlush(entity)
            logger.info("Successfully loaded ${file.name}!")
        } catch (e: Exception) {
            logger.error("An exception occurred while processing file $file. Make sure it follows correct naming: 'title_author.mp3/.wav'")
            moveAndCreateAccept(file, duration = duration)
        }
    }

    private fun moveAndCreateAccept(f: File, duration: Int) {
        logger.debug("Moving song to accept mode " + f.name)
        val target = File(SettingsService.acceptFolder, f.name)
        Files.move(f.toPath(), target.toPath())
        val accept = YoutubeEntry(target.nameWithoutExtension, "", "", target.name, arrayListOf(), userRepo.findOne(Constants.CONSOLE_USER), "", false, duration)
        youtubeRepo.saveAndFlush(accept)
        logger.info("Song ${accept.fullTitle} moved to accept mode!")
    }

    private fun rescanForSongs() {
        logger.trace("Rescanning songs DataBase..")
        SettingsService.songsFolder.mkdirs()

        val list = songsRepo.findAll()
        list.toTypedArray().forEach {
            val file = File(SettingsService.songsFolder, it.fileName)
            if (!file.exists()) {
                logger.error("${it.title}_${it.artist} not found! Deleting..")
                songsRepo.delete(it)
                list.remove(it)
            }
        }


        SettingsService.songsFolder.listFiles({ a -> !list.any { a == it.file } }).forEach { tryToLoad(f = it) }
        logger.info("Done accept songs scanning! Songs to accept: ${youtubeRepo.count()}")
    }

    private fun getDurationOfSong(file: File): Double {
        return try {
            val audioFile = AudioFileIO.read(file)
            audioFile.audioHeader.trackLength.toDouble()
        } catch (ex: Exception) {
            ex.printStackTrace()
            0.0
        }

    }

    fun getSongs(filter: String = "", page: Int = 0, size: Int = 10): Page<SongEntity> = songsRepo.findByTitleContaining(filter, PageRequest(page, size))

    fun removeSong(songNumber: Int): Boolean {
        removeSong(getSong(songNumber) ?: return false); return true
    }

    private fun removeSong(songEntity: SongEntity) {
        logger.debug("""Removing song ${songEntity.songName} from entity!""")
        Files.move(songEntity.file.toPath(), File(SettingsService.trashFolder, songEntity.fileName).toPath())
        musicService.songDeleting(songEntity)
        songsRepo.delete(songEntity)
    }

    fun setSongTitleAndAuthor(songEntity: SongEntity, title: String, author: String): SongEntity {
        songEntity.title = title
        songEntity.artist = author
        songsRepo.saveAndFlush(songEntity)
        return songEntity
    }

    fun getSong(songNumber: Int): SongEntity? = songsRepo.findOne(songNumber)

    fun declineSong(acceptId: Int) {
        logger.debug("Declining song!")
        SettingsService.trashFolder.mkdirs()
        val yt = youtubeRepo.findOne(acceptId) ?: return
        Files.move(yt.file.toPath(), File(SettingsService.trashFolder, yt.fileName).toPath())
        youtubeRepo.delete(yt)
    }

}
