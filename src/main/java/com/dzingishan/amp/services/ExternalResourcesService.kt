package com.dzingishan.amp.services

import com.dzingishan.amp.ProcessingException
import com.dzingishan.amp.musicanalysis.normalize
import com.dzingishan.amp.repositories.SongRepository
import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.YoutubeEntryRepository
import com.dzingishan.amp.repositories.entities.SongEntity
import com.dzingishan.amp.repositories.entities.UserEntity
import com.dzingishan.amp.repositories.entities.YoutubeEntry
import com.dzingishan.amp.utils.jsons.createPlaylist
import com.sapher.youtubedl.YoutubeDL
import com.sapher.youtubedl.YoutubeDLException
import com.sapher.youtubedl.YoutubeDLRequest
import com.wrapper.spotify.Api
import org.apache.commons.io.IOUtils
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import org.apache.log4j.Logger
import org.json.JSONException
import org.json.JSONObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.io.File
import java.io.IOException
import java.net.URLEncoder
import java.nio.charset.Charset
import java.nio.file.Files
import java.util.*
import javax.annotation.PostConstruct


private val clientId = "b8d2a1871bf840918ffedb9a673d1fc6"
private val clientSecret = "c6186a0a6e474716b2f155c1e1d66595"

@Service
open class ExternalResourcesService {

    @Autowired lateinit var userRepo: UserRepository
    @Autowired lateinit var youtubeEntryRepo: YoutubeEntryRepository
    @Autowired lateinit var songsRepo: SongRepository
    @Autowired lateinit var songsService: SongService

    @Autowired lateinit var downloadQueue: Queue<Pair<String, String>>

    private val api = Api.builder()
            .clientId(clientId)
            .clientSecret(clientSecret)
            .build()
    private var time = 0L

    private var logger = Logger.getLogger(ExternalResourcesService::class.qualifiedName)

    fun extractPlaylistLink(link: String) = link.substring(link.lastIndexOf("list=") + 5)

    private fun processSpotifyLink(url: String, user: UserEntity) {
        val playlist = getSpotifyPlaylist(url)
        val entries = playlist.filter { !songsRepo.existsByTitle(it.title) }.map {
            try {
                (it) to download(searchYoutube(it.title + " " + it.artist + " audio")[0].second, user)
            } catch (ex: Exception) {
                logger.error("Error occurred while parsing $it", ex)
                null
            }
        }
        entries.filterNotNull().forEach { acceptFile(it.second, it.first.title, it.first.artist) }
    }

    fun addToDownload(url: String, user: String): Boolean {
        logger.info("Downloading")
        val url =
                if (!isSpotifyLink(url) && !url.contains("youtube.com", true) && !url.contains("youtu.be")) {
                    searchYoutube(url)[0].second
                } else {
                    url
                }
        if (isYoutubePlaylistLink(url)) {
            val playlist = getYoutubePlaylistUrls(extractPlaylistLink(url))
            playlist.forEach { addToDownload(it, user) }
            return true
        }
        logger.info("Adding $url from user $user to download queue!")
        if (downloadQueue.any { it.first == url }) {
            logger.info("$url exists in database!")
            return false
        }
        if (!isSpotifyLink(url) && existsInDatabase(url)) {
            logger.info("$url exists in database!")
            return false
        }
        downloadQueue.offer(url to user)
        return true
    }

    private fun acceptFile(yt: YoutubeEntry, realTitle: String, realAuthor: String): Pair<Boolean, SongEntity?> {
        logger.info("Accepting song ${yt.fullTitle} to db")
        val song = yt.toSongEntity()
        if (realAuthor != "")
            song.artist = realAuthor
        else if (song.artist == "")
            return false to null
        if (realTitle != "")
            song.title = realTitle
        else if (song.artist == "")
            return false to null
        Files.move(yt.file.toPath(), song.file.toPath())
        youtubeEntryRepo.delete(yt)
        songsRepo.saveAndFlush(song)
        song.file = File(SettingsService.songsFolder, yt.file.name)
        logger.info("Successfully accepted $realTitle!")
        return true to song
    }

    fun acceptFile(id: Int, realTitle: String, realAuthor: String): Boolean {
        return acceptFile(youtubeEntryRepo.findOne(id) ?: return false, realTitle, realAuthor).first
    }

    @Scheduled(initialDelay = 1000, fixedRate = 1000)
    fun downloadThread() {
        if (System.currentTimeMillis() >= time) {
            logger.info("Refreshing local playlist token!")
            authorizeApi()
        }
        if (downloadQueue.isNotEmpty()) {
            while (downloadQueue.isNotEmpty()) {
                try {
                    val v = downloadQueue.poll()
                    logger.info("Downloading " + v.first)
                    if (isSpotifyLink(v.first)) {
                        processSpotifyLink(v.first, userRepo.findOne(v.second))
                        continue
                    }
                    val yt = download(v.first, userRepo.findOne(v.second))
                    normalize(yt.file)
                    youtubeEntryRepo.saveAndFlush(yt)
                    logger.info("Download complete! Please accept this song: " + yt.fullTitle)
                } catch (ex: Exception) {
                    logger.error("Exception", ex)
                    ex.printStackTrace()//TODO
                }
            }
        }
    }

    @PostConstruct
    private fun authorizeApi() {
        val client = api.clientCredentialsGrant().build().get()
        api.setAccessToken(client.accessToken)
        time = System.currentTimeMillis() + ((client.expiresIn - 600) * 1000)
    }

    private fun existsInDatabase(url: String): Boolean {
        val title = getTitle(getVideoID(url))
        if (songsRepo.existsByTitle(title) || youtubeEntryRepo.existsByFullTitle(title))
            return true
        return false
    }

    private fun download(url: String, user: UserEntity?, targetFormat: String = "wav"): YoutubeEntry {
        //TODO: No youtube dl = now download = download error
        val time = System.currentTimeMillis()
        logger.info("Downloading $url")
        val id = getVideoID(url)
        val urles = "https://youtube.com/watch?v=" + id
        try {
            val downloadResponse = (YoutubeDLRequest(urles))
            downloadResponse.option += hashMapOf("o" to File(SettingsService.acceptFolder, "%(title)s.%(ext)s").absolutePath, "extract-audio" to "", "audio-format" to targetFormat, "print-json" to "")
            val response = YoutubeDL.execute(downloadResponse)
            val o = JSONObject(response.out)
            val fullTitle = o.getString("fulltitle")
            val altTitle = o.get("alt_title")?.toString()?.replace("\"", "") ?: ""
            val creator = o.get("creator")?.toString() ?: ""
            val tags = o.getJSONArray("tags").toMutableList() as MutableList<String>
            val fileName = o.getString("_filename")
            val duration = o.getInt("duration")
            logger.info("Successfully downloaded $fullTitle in ${System.currentTimeMillis() - time}")
            return YoutubeEntry(fullTitle = fullTitle ?: "", title = altTitle, author = creator, fileName = File(fileName.replace(".webm", "." + targetFormat).replace(".mp4", "." + targetFormat).replace(".flv", "." + targetFormat).replace(".m4a", "." + targetFormat)).name, tags = tags, user = user, url = urles, duration = duration)
        } catch (e: YoutubeDLException) {
            throw ProcessingException(error = "download_error", throwable = e)
        }

    }

    private fun getVideoID(url: String): String {
        logger.trace("Getting videoID of $url")
        return Regex(pattern = "(\\?v=)...........").find(url, 0)?.value?.replace("?v=", "")!!
    }

    private fun getTitle(videoId: String): String {
        logger.trace("Getting TITLE of $videoId")
        return try {
            val obj = request("https://www.googleapis.com/youtube/v3/videos?part=snippet&id=$videoId&fields=items%2Fsnippet%2Ftitle&key=AIzaSyD5cJi-HJUNzrKgnq1LKbBK7KgT3LkfWE0")
            obj.getJSONArray("items").getJSONObject(0).getJSONObject("snippet").getString("title")
        } catch (ex: IOException) {
            logger.error("Error", ex)
            ex.printStackTrace()
            ""
        }

    }

    @Throws(IOException::class)
    private fun request(url: String): JSONObject {
        logger.trace("Request to $url")
        HttpClients.createDefault().use { client ->
            val request = HttpGet(url)
            val res = client.execute(request)
            val s = IOUtils.toString(res.entity.content, Charset.defaultCharset())
            return JSONObject(s)
        }
    }


    val songsToAccept: List<YoutubeEntry>
        get() = youtubeEntryRepo.findAll()

    fun searchForLyricsVideo(yt: YoutubeEntry, u: UserEntity?) {
        val l = searchYoutube(yt.fullTitle + " lyrics")[0].second
        val y = download(l, u ?: yt.user)
        yt.file.delete()
        yt.fileName = y.fileName
    }

    fun searchForLyricsVideo(song: SongEntity, u: UserEntity?) {
        val l = searchYoutube(song.songName + " lyrics")[0].second
        val y = download(l, u ?: userRepo.findOne(song.creator))
        song.file.delete()
        song.fileName = y.fileName
        Files.move(y.file.toPath(), song.file.toPath())
    }

    fun searchYoutube(keyword: String): List<Pair<String, String>> {
        logger.info("Searching youtube for $keyword")
        try {
            val obj = request("https://www.googleapis.com/youtube/v3/search?part=snippet&maxResults=20&fields=items(id(videoId)%2C+snippet(title%2C+channelTitle))&key=AIzaSyD5cJi-HJUNzrKgnq1LKbBK7KgT3LkfWE0&q=" + URLEncoder.encode(keyword, "UTF-8"))
            val arra = obj.getJSONArray("items")
            val entries = mutableListOf<Pair<String, String>>()
            for (i in 0 until arra.length()) {
                try {
                    val o = arra.getJSONObject(i)
                    val videoID = "https://www.youtube.com/watch?v=" + o.getJSONObject("id").getString("videoId")
                    val snippet = o.getJSONObject("snippet")
                    val title = snippet.getString("title")
                    entries.add(title to videoID)
                } catch (ex: JSONException) {
                }

            }
            return entries
        } catch (ex: IOException) {
            logger.info("Error while searching youtube")
            return listOf()
        }

    }

    fun isYoutubePlaylistLink(link: String): Boolean = (link.contains("list=") && !link.contains("watch?v="))

    fun getYoutubePlaylistUrls(playlistID: String): List<String> {
        val link = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&maxResults=50&fields=items(snippet(resourceId%2FvideoId%2Ctitle))%2CnextPageToken%2CpageInfo&key=AIzaSyD5cJi-HJUNzrKgnq1LKbBK7KgT3LkfWE0&playlistId=" + playlistID
        val list = mutableListOf<String>()
        var objectsInList = 0
        var currentObjectCount = 0
        try {
            var current = request(link)
            do {
                if (objectsInList == 0) {
                    objectsInList = current.getJSONObject("pageInfo").getInt("totalResults")
                }
                val jsonArray = current.getJSONArray("items")
                for (i in 0 until jsonArray.length()) {
                    list.add("https://www.youtube.com/watch?v=" + jsonArray.getJSONObject(i).getJSONObject("snippet").getJSONObject("resourceId").getString("videoId"))
                    currentObjectCount++
                }
                if (currentObjectCount != objectsInList) {
                    val next = current.getString("nextPageToken")
                    current = request(link + "&pageToken=" + next)
                } else {
                    break
                }
            } while (currentObjectCount < objectsInList)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return list
    }

    private fun extractUserAndPlaylist(url: String): Pair<String, String> {
        val split = url.split("/")
        val start = split.indexOf("user")
        val user = split[start + 1]
        val playlist = split[split.size - 1].substring(0, 22)
        println(user + " " + playlist)
        return user to playlist
    }

    fun isSpotifyLink(url: String) = url.toLowerCase().contains("spotify.com")

    fun getSpotifyPlaylist(url: String): List<SongData> {
        val users = extractUserAndPlaylist(url)
        return getSpotifyPlaylist(users.first, users.second)
    }

    private fun getSpotifyPlaylist(user: String, playlistId: String): List<SongData> { //TODO: System preventing the errors
        val api = Api.builder()
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build()
        val client = api.clientCredentialsGrant().build().get()
        api.setAccessToken(client.accessToken)
        val play = createPlaylist(net.sf.json.JSONObject.fromObject(api.getPlaylist(user, playlistId).build().json)).tracks.items
        return play.map { SongData(it.track.name, it.track.artists[0].name) }
    }

    private fun getSpotifySong(query: String): SongData {
        val api = Api.builder()
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build()
        val client = api.clientCredentialsGrant().build().get()
        api.setAccessToken(client.accessToken)
        val list = api.searchTracks(query).build().get().items
        return SongData(list[0].name, list[0].artists[0].name)
    }

    fun getFeaturedPlaylists(): List<FeaturedPlaylist> {
        val api = Api.builder()
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build()
        val client = api.clientCredentialsGrant().build().get()
        api.setAccessToken(client.accessToken)
        val list = api.featuredPlaylists.build().get().playlists.items
        return list.map {
            if (it.externalUrls.externalUrls.containsKey("spotify"))
                FeaturedPlaylist(it.name, it.externalUrls.get("spotify"), getSpotifyPlaylist("spotify", it.id))
            else (null)
        }.filter { it != null }.filterNotNull()
    }

    private fun getSongByFileAndAccept(title: String = "", author: String = "", url: String = "", autoAccept: Boolean = true): SongEntity? {
        val downloadResponse = (YoutubeDLRequest(if (url == "") searchYoutube("$title $author lyrics")[0].second else url))
        downloadResponse.option += hashMapOf("o" to File(SettingsService.acceptFolder, "%(title)s.%(ext)s").absolutePath, "extract-audio" to "", "audio-format" to "wav", "print-json" to "", "simulate" to "")
        val response = YoutubeDL.execute(downloadResponse)
        val o = JSONObject(response.out)
        val fileName = o.getString("_filename")
        val name = fileName.replace(".webm", "").replace(".mp4", "").replace(".flv", "").replace(".m4a", "")
        val wavAccept = (File(name + ".wav"))
        val mp3Accept = File(name + ".mp3")
        val wav = File(SettingsService.songsFolder, File(name + ".wav").name)
        val mp3 = File(SettingsService.songsFolder, File(name + ".mp3").name)
        return when {
            wavAccept.exists() && autoAccept -> acceptFile(youtubeEntryRepo.findOneByFileName(wavAccept.name) ?: return null, title, author).second
            mp3Accept.exists() && autoAccept -> acceptFile(youtubeEntryRepo.findOneByFileName(mp3Accept.name) ?: return null, title, author).second
            wav.exists() -> songsService.setSongTitleAndAuthor(songsRepo.findOneByFileName(wav.name) ?: return null, title, author)
            mp3.exists() -> songsService.setSongTitleAndAuthor(songsRepo.findOneByFileName(mp3.name) ?: return null, title, author)
            else -> null
        }
    }

    private fun getSongByTitleAndAuthor(title: String, author: String)
            = songsRepo.findOneByAuthorTitle(title, author)

    fun getInAnyMethodSpotify(title: String, author: String, user: UserEntity? = null): SongEntity {
        var sE = (getSongByTitleAndAuthor(title, author))
        if (sE != null) {
            logger.debug("Found $title by title & author"); return sE
        }
        val songYTUrl = searchYoutube("$title $author lyrics")[0].second
        sE = getSongByURLAndAccept(songYTUrl, title, author)
        if (sE != null) {
            logger.debug("Found $title by URL"); return sE
        }
        sE = getSongByFileAndAccept(title, author, songYTUrl)
        if (sE != null) {
            logger.debug("Found $title by File"); return sE
        }
        logger.debug("$title not found! Downloading from youtube...")
        return searchAndDownload(title, author, user, songYTUrl)
    }

    fun getInAnyMethodYoutube(url: String, user: UserEntity? = null): SongEntity? {
        var sE = getSongByURLAndAccept(songYTUrl = url, autoAccept = false)
        if (sE != null) {
            logger.debug("Found $url by URL"); return sE
        }
        sE = getSongByFileAndAccept(url = url, autoAccept = false)
        if (sE != null) {
            logger.debug("Found $url by File"); return sE
        }
        return null
    }

    private fun getSongByURLAndAccept(songYTUrl: String, title: String = "", author: String = "", autoAccept: Boolean = true): SongEntity? {
        val song = songsRepo.findOneByUrl(songYTUrl)
        if (song != null) {
            if (autoAccept)
                songsService.setSongTitleAndAuthor(song, title, author)
            return song
        }
        val s = youtubeEntryRepo.findOneByUrl(songYTUrl)
        if (s != null && !autoAccept) {
            return acceptFile(s, title, author).second
        }
        return null
    }


    private fun searchAndDownload(title: String, author: String, user: UserEntity? = null, url: String = "") =
            acceptFile(download(if (url == "") searchYoutube("$title $author lyrics")[0].second else url, user), title, author).second ?: throw ProcessingException("Error in downloading song!")
}

data class FeaturedPlaylist(val title: String, val url: String, val songs: List<SongData>)
data class SongData(val title: String, val artist: String)