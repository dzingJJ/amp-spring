package com.dzingishan.amp.services

import com.dzingishan.amp.kotlin.Constants
import com.dzingishan.amp.repositories.GroupRepository
import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.entities.GroupEntity
import com.dzingishan.amp.repositories.entities.UserEntity
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.annotation.PostConstruct

@Service
class GroupAndUsersService {

    @Autowired
    lateinit var groupRepository: GroupRepository

    @Autowired private lateinit var userRepository: UserRepository

    private var logger = Logger.getLogger(GroupAndUsersService::class.qualifiedName)

    @PostConstruct
    fun start() {
        if (!userRepository.exists(Constants.CONSOLE_USER)) {
            logger.info("Creating console user...")
            userRepository.saveAndFlush(UserEntity("console", "console", "console", "this is console", mutableSetOf<GroupEntity>(), mutableListOf("*"), "", "console"))
        }
        if (!userRepository.exists(Constants.GUEST_USER)) {
            logger.info("Creating guest user...")
            userRepository.saveAndFlush(UserEntity("guest", "guest", "guest", "this is guest", mutableSetOf<GroupEntity>(), mutableListOf("request.public.*", "request.login.*", "request.base"), "", "guest"))

        }
        if (!groupRepository.existsByGroupName("guest")) {
            logger.info("Group guest not exists, creating default ones")
            val userGroup = GroupEntity()
            userGroup.groupName = "guest"
            userGroup.permissions += mutableListOf("request.public.*", "request.login.*", "request.base")
            groupRepository.saveAndFlush(userGroup)
        }
    }

    val defaultGroup: GroupEntity
        get() = groupRepository.getOne(Constants.DEFAULT_GUEST_GROUP_NAME)


}
