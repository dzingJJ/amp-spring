package com.dzingishan.amp.services

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.log4j.Logger
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.io.File
import javax.annotation.PostConstruct
import javax.annotation.PreDestroy

@Service
open class SettingsService {

    private val mapper = ObjectMapper()
    private val file = File("config.json")

    private var logger = Logger.getLogger(SettingsService::class.qualifiedName)

    @PostConstruct
    fun load() {
        if (file.exists()) {
            logger.info("Loading Settings...")
            settings = mapper.readValue<SettingsContainer>(file, SettingsContainer::class.java)
        }
    }

    @Scheduled(initialDelay = 1000, fixedRate = 300000)
    private fun save() {
        logger.info("Saving Settings...")
        mapper.writeValue(file, settings)
    }

    @PreDestroy
    private fun destroy() {
        save()
    }


    companion object {
        private var settings = SettingsContainer()
        var trashFolder
            get() = settings.trashFolder
            set(value) {
                settings.trashFolder = value
            }
        var timeZone
            get() = settings.timeZone
            set(value) {
                settings.timeZone = value
            }
        var songsFolder
            get() = settings.songsFolder
            set(value) {
                settings.songsFolder = value
            }
        var randomPlaying
            get() = settings.randomPlaying
            set(value) {
                settings.randomPlaying = value
            }
        var globalIgnore
            get() = settings.globalIgnore
            set(value) {
                settings.globalIgnore = value
            }
        var globalLock
            get() = settings.globalLock
            set(value) {
                settings.globalLock = value
            }
        var keyPersistTime
            get() = settings.keyPersistTime
            set(value) {
                settings.keyPersistTime = value
            }
        var acceptFolder
            get() = settings.acceptFolder
            set(value) {
                settings.acceptFolder = value
            }
        var secondsDelay
            get() = settings.secondsDelay
            set(value) {
                settings.secondsDelay = value
            }
        var stop
            get() = settings.stop
            set(value) {
                settings.stop = value
            }
        var dummyMode
            get() = settings.dummyMode
            set(value) {
                settings.dummyMode = value
            }
        var youtubeDownloadWorks = false
        var mp3GainWorks = false
        var normalizerWorks = false
    }

}

class SettingsContainer(
        var songsFolder: File = File("songs"),
        var randomPlaying: Boolean = false,
        var globalIgnore: Boolean = false,
        var globalLock: Boolean = false,
        var keyPersistTime: Long = 129600000L,
        var acceptFolder: File = File("accept"),
        var timeZone: Int = 0,
        var secondsDelay: Int = 0,
        var stop: Boolean = false,
        var trashFolder: File = File("thrash"),
        var dummyMode: Boolean = false
)

