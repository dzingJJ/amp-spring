package com.dzingishan.amp.spring

//import com.dzingishan.amp.kotlin.Kotlin
import com.dzingishan.amp.repositories.GroupRepository
import com.dzingishan.amp.repositories.ProgramDaysRepository
import com.dzingishan.amp.repositories.SongRepository
import com.dzingishan.amp.repositories.UserRepository
import javazoom.jlgui.basicplayer.BasicPlayer
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.web.servlet.config.annotation.EnableWebMvc
import java.util.*
import java.util.concurrent.LinkedTransferQueue

@Configuration
@EnableScheduling
@EnableWebMvc
@EnableAsync
@EnableAspectJAutoProxy
@ComponentScan("com.dzingishan.amp.components", "com.dzingishan.amp.repositories", "com.dzingishan.amp.repositories.entities", "com.dzingishan.amp.services", "com.dzingishan.amp.shell", "com.dzingishan.amp.security")
@EnableJpaRepositories(basePackageClasses = [(UserRepository::class), (GroupRepository::class), (SongRepository::class), (ProgramDaysRepository::class)])//TODO
@EntityScan("com.dzingishan.amp.repositories.entities")
open class Configurator {


    @Bean
    open fun getBasicPlayer(): BasicPlayer = BasicPlayer()

    @Bean
    open fun getStringList(): List<String> = mutableListOf()

    @Bean
    open fun getStringPairQueue(): Queue<Pair<String, String>> = LinkedTransferQueue<Pair<String, String>>()
}
