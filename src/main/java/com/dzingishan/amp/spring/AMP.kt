package com.dzingishan.amp.spring

import com.dzingishan.amp.services.SettingsService
import org.apache.log4j.Logger
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.Scheduled

@SpringBootApplication
open class AMP {
    val logger = Logger.getLogger(AMP::class.qualifiedName)

    @Scheduled(fixedDelay = 10000)
    fun infoTick() {
        if (SettingsService.dummyMode) {
            logger.info("Dummy mode is activated!")
        }
        if (SettingsService.globalLock) {
            logger.info("Global Lock is activated!")
        }
    }


}