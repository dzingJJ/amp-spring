package com.dzingishan.amp.musicanalysis

import com.dzingishan.amp.ProcessingException
import com.dzingishan.amp.services.LibrariesService
import com.dzingishan.amp.services.SettingsService
import org.apache.log4j.Logger
import java.io.File

var logger = Logger.getLogger(LibrariesService::class.qualifiedName)

fun normalize(file: File) {
    when {
        file.extension == "wav" -> normalizeWavGain(file)
        file.extension == "mp3" -> normalizeMP3Gain(file)
        else -> throw ProcessingException("Only mp3/wav files are supported!")
    }
}

private fun normalizeMP3Gain(file: File) {
    if (SettingsService.mp3GainWorks) {
        val process = ProcessBuilder(LibrariesService.MP3GAIN_PATH, "/r", file.absolutePath)
        process.inheritIO()//TODO
        process.start()
    } else {
        logger.warn("MP3 Normalizing disabled! Please install mp3-gain")
    }
}

private fun normalizeWavGain(file: File) {
    if (SettingsService.normalizerWorks) {
        val process = ProcessBuilder(LibrariesService.NORMALIZE_PATH, file.absolutePath)
        process.inheritIO() //TODO
        process.start()
    } else {
        logger.warn("WAV Normalizing disabled. Please install normalize-audio or normalize")
    }
}