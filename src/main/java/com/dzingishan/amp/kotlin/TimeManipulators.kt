/*
 * Copyright (c) 2017. Grzegorz Dzikowski
 * All Rights Reserved.
 * Any modification, copying, redistribution is strictly forbidden
 */

@file:JvmName("Kotlin")

package com.dzingishan.amp.kotlin

import com.dzingishan.amp.repositories.SongRepository
import com.dzingishan.amp.services.MusicService
import org.springframework.beans.factory.annotation.Autowired
import java.text.ParseException
import java.time.*
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeParseException

/**
 * Created by Grzegorz on 24.01.2017.
 */

class TimeManipulators {

    @Autowired
    lateinit var songRepository: SongRepository

    @Autowired
    lateinit var musicService: MusicService


    companion object {

        val STATIC_TIME_MAX: Int = 604799
        val EVERY_DAY_OF_WEEK: Int = 86400

        fun currentTimeWithTimeZoneSeconds(): Long = Instant.now(Clock.system(ZoneId.of("Europe/Warsaw"))).atZone(ZoneId.of("Europe/Warsaw")).toLocalDateTime().toEpochSecond(ZoneOffset.UTC)

        fun currentStaticTime(): Int = fromMillisToStatic(currentTimeWithTimeZoneSeconds())

        fun toReadable(time: Long): String = LocalDateTime.ofEpochSecond(time, 0, ZoneOffset.ofHours(0)).format(DateTimeFormatter.ofPattern("dd:MM:yyyy - HH:mm:ss"))

        fun fromMillisToStatic(time: Long): Int {
            val instant = LocalDateTime.ofEpochSecond(time, 0, ZoneOffset.ofHours(0))
            val dayOfWeek = (instant.dayOfWeek.value - 1)
            val hour = instant.hour
            val add = hour * 3600 + (instant.minute * 60) + instant.second
            return dayOfWeek * EVERY_DAY_OF_WEEK + add
        }

        @Throws(ParseException::class)
        fun parseStaticTimer(arg: String): Int {
            val split = arg.split("_").toTypedArray()
            val time = try {
                LocalTime.parse(split[0], DateTimeFormatter.ofPattern("HH:mm:ss"))
            } catch (ex: DateTimeParseException) {
                try {
                    LocalTime.parse(split[0], DateTimeFormatter.ofPattern("HH:mm"))
                } catch (ex: DateTimeParseException) {
                    throw ParseException("Incorrect Date format: HH:mm or HH:mm:ss", 3)
                }
            }
            val timed = time.toSecondOfDay()
            val week = if (split.size == 1) (LocalDateTime.now(ZoneOffset.ofHours(2).normalized())).dayOfWeek.value - 1 else {
                val wS = split[1]
                when {
                    wS.equals("mon", true) -> 0
                    wS.equals("tue", true) -> 1
                    wS.equals("wed", true) -> 2
                    wS.equals("thu", true) -> 3
                    wS.equals("fri", true) -> 4
                    wS.equals("sat", true) -> 5
                    wS.equals("sun", true) -> 6
                    else -> throw ParseException("Unknown day of week $wS", 2)
                }
            }
            return ((EVERY_DAY_OF_WEEK * week) + timed)
        }

        fun toReadableFromStatic(time: Int): String {
            val asd: Int = ((time / EVERY_DAY_OF_WEEK) % (7))
            var s = "UNK"
            when (asd) {
                0 -> s = "Mon"
                1 -> s = "Tue"
                2 -> s = "Wed"
                3 -> s = "Thu"
                4 -> s = "Fri"
                5 -> s = "Sat"
                6 -> s = "Sun"
            }
            return "${s}_${LocalTime.ofSecondOfDay(time.toLong() - asd * EVERY_DAY_OF_WEEK).format(DateTimeFormatter.ofPattern("HH:mm:ss"))}"
        }
    }
}