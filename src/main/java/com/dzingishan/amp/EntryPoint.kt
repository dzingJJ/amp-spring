@file:JvmName("EntryPoint")
package com.dzingishan.amp

import com.dzingishan.amp.spring.AMP
import org.springframework.boot.SpringApplication

/**
 * This is the entry point of application - here it starts
 */
fun main(args: Array<String>) {
    SpringApplication.run(AMP::class.java)
}

