package com.dzingishan.amp.security

import com.dzingishan.amp.kotlin.Constants
import com.dzingishan.amp.musicanalysis.logger
import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.entities.UserEntity
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.AbstractAuthenticationToken
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


class AuthFilter : OncePerRequestFilter() {

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest,
                                  response: HttpServletResponse, filterChain: FilterChain) {
        val xAuth = request.getHeader("Auth-Key")
        var s = "request." + request.servletPath.substring(1).replace("/", ".")
        if (s.endsWith('.'))
            s = s.substring(0, s.length - 1)
        if (s == "request.") {
            s = "request.base"
        }

        val auth = TempAuth(key = xAuth, path = s)
        SecurityContextHolder.getContext().authentication = auth
        filterChain.doFilter(request, response)
    }


}

data class TempAuth(var key: String?, var path: String) : AbstractAuthenticationToken(null) {
    override fun getCredentials(): Any = Any()

    override fun getPrincipal(): Any = Any()
}

data class KeyAuthentication(var user: UserEntity, var bool: Boolean = false) : Authentication {


    override fun getAuthorities(): MutableCollection<out GrantedAuthority>? = null

    override fun setAuthenticated(p0: Boolean) {
    }

    override fun getName(): String = user.username

    override fun getCredentials(): Any = Any()

    override fun getPrincipal(): Any = user

    override fun isAuthenticated(): Boolean = true

    override fun getDetails(): Any? = null

}


@Component
class UserAuthProvider : AuthenticationProvider {

    @Autowired
    private lateinit var authService: AuthenticatorService

    @Autowired private lateinit var userRepo: UserRepository

    private var logger = Logger.getLogger(UserAuthProvider::class.qualifiedName)

    @Throws(AuthenticationException::class)
    override fun authenticate(authentication: Authentication): Authentication? {
        val auth = authentication as TempAuth
        if (auth.key == null) {
            auth.key = ""
        }
        var u = authService.getUserEntityByKey(auth.key!!)
        if (u == null) {
            u = userRepo.findOneByUsername(Constants.GUEST_USER)!!
        }
        return if (!u.hasPermission(auth.path)) {
            logger.info("User ${u.username} used ${auth.path} and have no permission for it")
            null
        } else {
            logger.info("User ${u.username} used ${auth.path} and is allowed to do it")
            KeyAuthentication(u, true)
        }
    }

    override fun supports(authentication: Class<*>): Boolean = TempAuth::class.java.isAssignableFrom(authentication)

}


@Component
class CustomAccessDeniedHandler : AccessDeniedHandler {
    override fun handle(request: HttpServletRequest, response: HttpServletResponse, ex: org.springframework.security.access.AccessDeniedException?) {
        logger.error("CUSTOM ACCESS DENIED HANDLER REQUEST")
    }


}