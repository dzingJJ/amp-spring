package com.dzingishan.amp.security

import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.entities.UserEntity
import com.dzingishan.amp.services.SettingsService
import org.apache.commons.lang.RandomStringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service


@Service
open class AuthenticatorService {

    @Autowired private lateinit var userRepo: UserRepository

    private var keyMap: List<AuthKey> = mutableListOf()

    init {
        keyMap += AuthKey("wEaReNumBAS123!@#%", createTime = Long.MAX_VALUE, user = "console")
    }
    @Scheduled(fixedRate = 10000)
    private fun authTick() {
        keyMap -= keyMap.filter { System.currentTimeMillis() - it.createTime > SettingsService.keyPersistTime }
    }

    private fun getUserByKey(key: String): String? = keyMap.firstOrNull { it.key == key }?.user

    fun getUserEntityByKey(key: String): UserEntity? {
        val s = getUserByKey(key)
        return if (s != null)
            userRepo.findOneByUsername(s)
        else
            null
    }

    fun isKeyCorrect(key: String): Boolean = keyMap.any { it.key == key }

    fun login(user: UserEntity): String {
        val randKey = RandomStringUtils.randomAlphanumeric(16)
        keyMap += AuthKey(randKey, user = user.username)
        return randKey
    }

    fun logout(key: String?) {
        keyMap -= keyMap.firstOrNull { it.key == key } ?: AuthKey("", user = "")
    }


}

data class AuthKey(val key: String, val createTime: Long = System.currentTimeMillis(), val user: String)