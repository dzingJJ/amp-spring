package com.dzingishan.amp.security

import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter


@EnableWebSecurity
open class SecurityConfiguration : WebSecurityConfigurerAdapter() {

    @Autowired
    private lateinit var accessDeniedHandler: CustomAccessDeniedHandler

    @Autowired
    private lateinit var demoAuthenticationProvider: UserAuthProvider

    private var logger = Logger.getLogger(SecurityConfiguration::class.qualifiedName)

    @Throws(Exception::class)
    override fun configure(http: HttpSecurity) {
        logger.debug("Security configuration..")
        http.csrf().disable().addFilterBefore(AuthFilter(), BasicAuthenticationFilter::class.java).authorizeRequests().antMatchers("/**").authenticated()
                .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler).accessDeniedPage("/403")
    }


    @Throws(Exception::class)
    public override fun configure(auth: AuthenticationManagerBuilder?) {
        logger.debug("Configuration of AuthManager")
        auth!!.authenticationProvider(demoAuthenticationProvider)
    }
}