package com.dzingishan.amp.events

import com.dzingishan.amp.services.ScheduledSong

data class TickEvent(var continueOperation: Boolean = true)

data class MusicPlayEvent(var scheduledSong: ScheduledSong)

data class MusicStopEvent(var scheduledSong: ScheduledSong)
