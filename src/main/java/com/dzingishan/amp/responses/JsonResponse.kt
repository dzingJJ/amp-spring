package com.dzingishan.amp.responses

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.ui.ModelMap
import org.springframework.web.servlet.ModelAndView

class JsonResponse<T>(var response: String = "ok", var additionalData: Array<String> = emptyArray(), var data: T? = null, status: HttpStatus = HttpStatus.OK) : ResponseEntity<ModelMap>(status) {


//    private var json: String = if (data != null) {
//        ObjectMapper().writeValueAsString(data)
//    } else {
//        "";
//    }

    override fun getBody(): ModelMap = ModelAndView().addObject("response", response).addObject("json", data).addObject("additionalData", additionalData).modelMap

    fun setData(array: Array<String>): JsonResponse<T> {
        additionalData = array
        return this
    }

    companion object {

        val OK_RESPONSE = JsonResponse<Any>("ok")
        val PROCESS_ERROR = JsonResponse<Any>("process_error")
    }

}
