package com.dzingishan.amp

import com.dzingishan.amp.responses.JsonResponse
import org.springframework.http.HttpStatus

/**
 * Exception thrown when an error during processing request occurred
 * Can contains [JsonResponse] as an response to error
 */
class ProcessingException(error: String, data: Any? = null, additional: Array<String>? = null, throwable: Throwable? = null) : RuntimeException(error, throwable) {

    var response: JsonResponse<Any>

    init {
        var arrayAdditional = additional
        if (arrayAdditional == null) {
            arrayAdditional = if (throwable != null)
                arrayOf(throwable.message!!)
            else arrayOf()
        }
        response = JsonResponse(error, arrayAdditional, data, HttpStatus.BAD_REQUEST)
    }

}
