package com.dzingishan.amp.shell

import com.dzingishan.amp.kotlin.Constants
import com.dzingishan.amp.kotlin.Constants.CONSOLE_USER
import com.dzingishan.amp.kotlin.TimeManipulators
import com.dzingishan.amp.kotlin.TimeManipulators.Companion.toReadable
import com.dzingishan.amp.kotlin.TimeManipulators.Companion.toReadableFromStatic
import com.dzingishan.amp.musicanalysis.normalize
import com.dzingishan.amp.repositories.PlaylistRepository
import com.dzingishan.amp.repositories.ProgramDaysRepository
import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.entities.ProgramDayEntity
import com.dzingishan.amp.repositories.entities.VolumeEntry
import com.dzingishan.amp.services.ExternalResourcesService
import com.dzingishan.amp.services.MusicService
import com.dzingishan.amp.services.SettingsService
import com.dzingishan.amp.services.SongService
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod
import org.springframework.shell.standard.ShellOption
import java.text.ParseException

@ShellComponent
open class ShellCommandsComponent {

    @Autowired
    private lateinit var songsService: SongService
    @Autowired
    private lateinit var playlist: PlaylistRepository
    @Autowired private lateinit var programWeekRepo: ProgramDaysRepository

    @Autowired private lateinit var musicService: MusicService
    @Autowired private lateinit var userRepo: UserRepository
    @Autowired private lateinit var externalResourcesService: ExternalResourcesService
    @Autowired private lateinit var playlistRepo: PlaylistRepository

    @Autowired private lateinit var settingsService: SettingsService

    private var logger = Logger.getLogger(ShellCommandsComponent::class.qualifiedName)

    @ShellMethod("Reload config")
    fun reload(): String {
        logger.trace("Reload command")
        settingsService.load()
        return "Loaded"
    }

    @ShellMethod("This is function which does something")
    fun play(
            name: String
    ): String = "works with " + name

    @ShellMethod("Adds song to queue")
    fun addSong(@ShellOption(arity = 1) songNumber: Int): String {
        logger.trace("AddSong command: $songNumber")
        val song = songsService.getSong(songNumber)
        return if (!musicService.scheduleSong(songNumber, userRepo.findOneByUsername(Constants.GUEST_USER))) {
            ("Song not found!")
        } else {
            "Scheduled " + song?.title + " to system!"
        }

    }

    @ShellMethod("Gets list of songs")
    fun songs(@ShellOption(defaultValue = "") filter: String, @ShellOption(defaultValue = "0") page: Int, @ShellOption(value = ["--records"], defaultValue = "10") recordPerPage: Int): List<String> {
        logger.trace("Songs command $filter $page $recordPerPage ")
        val pages = songsService.getSongs(page = page, size = recordPerPage, filter = filter)
        val list = mutableListOf<String>()
        pages.content.forEach { list.add("ID:${it.numberId}. ${it.title} - ${it.artist}") }
        list.add("Total Pages: ${pages.totalPages}")
        list.add("Total Songs: ${pages.totalElements}")
        return list

    }

    @ShellMethod("Add entry to program")
    fun addTime(startTime: String, endTime: String): String {
        logger.trace("AddTime command: $startTime $endTime")
        return try {
            val startTimes = TimeManipulators.parseStaticTimer(startTime)
            val endTimes = TimeManipulators.parseStaticTimer(endTime)
            programWeekRepo.saveAndFlush(ProgramDayEntity(creator = CONSOLE_USER, relativeStartTime = startTimes, relativeEndTime = endTimes))
            "Successfully added time!"
        } catch (e: ParseException) {
            e.printStackTrace()
            "Error while processing time"
        }
    }

    @ShellMethod("Sets volume")
    fun vol(@ShellOption(defaultValue = "0.0") vol: Double): String {
        logger.trace("Vol command: $vol")
        return if (vol == 0.0) {
            "Current Volume: " + musicService.volume
        } else {
            musicService.volume = vol
            "Volume set!"
        }
    }

    @ShellMethod("Emergency STOP")
    fun emergency(): String {
        logger.trace("Emergency command")
        SettingsService.globalLock = !SettingsService.globalLock
        if (SettingsService.globalLock) {
            clean()
        }
        return "Current lock status: " + SettingsService.globalLock
    }

    @ShellMethod("Clears the playlist")
    fun clean(): String {
        logger.trace("Clean command")
        playlist.deleteAll()
        return "Deleted while playlist!"
    }

    @ShellMethod("Downloads new song")
    fun download(url: String): String {
        logger.trace("Url command: $url")
        externalResourcesService.addToDownload(url, Constants.CONSOLE_USER)
        return "Planned song to download"
    }

    @ShellMethod("Download List")
    fun downloadList(): List<String> {
        logger.trace("DownloadList Command")
        return externalResourcesService.downloadQueue.map { it.first }
    }

    @ShellMethod("Displays List of Songs to Accept")
    fun acceptList(): List<String> {
        logger.trace("AcceptList command!")
        return listOf("LIST OF SONGS TO ACCEPT:", "--------------------------") + externalResourcesService.songsToAccept.map { it -> "${it.id} -> ${it.fullTitle}" }
    }

    @ShellMethod("accepts song")
    fun accept(id: Int, targetTitle: String, author: String): String {
        logger.trace("Accept command: $id $targetTitle $author")
        return if (externalResourcesService.acceptFile(id, targetTitle, author)) "Operation Success!" else "Operation failed"
    }

    @ShellMethod("Rescans songs!")
    fun rescan(): String {
        logger.trace("Rescan command")
        songsService.rescanAll()
        return "RESCANNING SONGS"
    }

    @ShellMethod("Cleans the days program")
    fun cleanProgram(): String {
        logger.trace("CleanProgram command")
        programWeekRepo.deleteAll()
        return "Cleaned Week Program!"
    }

    @ShellMethod("Adds program Entry every day")
    fun addProgramEveryday(noWeekends: Boolean, start: String, end: String): String {
        logger.trace("AddProgramEveryday command: $noWeekends $start $end")
        val timerStart = start + "_mon"
        val timerEnd = end + "_mon"
        val staticTimeStart = TimeManipulators.parseStaticTimer(timerStart)
        val staticTimeEnd = TimeManipulators.parseStaticTimer(timerEnd)
        if (staticTimeStart > TimeManipulators.EVERY_DAY_OF_WEEK) {
            return "Invalid time"
        }
        (0..if (noWeekends) 4 else 6)
                .map { ProgramDayEntity(creator = Constants.CONSOLE_USER, relativeStartTime = staticTimeStart + TimeManipulators.EVERY_DAY_OF_WEEK * it, relativeEndTime = staticTimeEnd + TimeManipulators.EVERY_DAY_OF_WEEK * it) }
                .forEach { programWeekRepo.saveAndFlush(it) }
        return "Adding complete"
    }

    @ShellMethod("Stop music")
    fun stop(): String {
        logger.trace("Stop command")
        musicService.stop()
        return "Completed"
    }

    @ShellMethod("Normalize All Songs")
    fun normalizeAll(): String {
        logger.trace("NormalizeAll command")
        songsService.songsRepo.findAll().forEach { logger.info("Normalizing " + it.songName); normalize(it.file); }
        songsService.youtubeRepo.findAll().forEach { logger.info("Normalizing " + it.fullTitle); normalize(it.file); }
        return "Normalized all songs!"
    }

    @ShellMethod("Playlist")
    fun playlist(): List<String> {
        logger.trace("Playlist Command")
        return playlistRepo.findAll().sortedBy { it.number }.map { it -> """${it.number}.${it.songEntity?.title} - ${it.songEntity?.artist}""" }
    }

    @ShellMethod("Current time")
    fun time(): String = toReadable(TimeManipulators.currentTimeWithTimeZoneSeconds())

    @ShellMethod("set zone")
    fun zone(zone: Int): String {
        SettingsService.timeZone = zone
        return "SET ZONE"
    }

    @ShellMethod("Current Static time")
    fun staticTime(): String = "${TimeManipulators.currentStaticTime()}"

    @ShellMethod("Status")
    fun status(): List<String> {
        return arrayListOf("TODO: Status", "Libs status: ",
                "youtube-dl: ${SettingsService.youtubeDownloadWorks}",
                "mp3gain: ${SettingsService.mp3GainWorks}",
                "normalize-audio: ${SettingsService.normalizerWorks}",
                "time: ${time()}")
    }


    @ShellMethod("Search and downloads lyrics version of song")
    fun lyricsSong(song: Int): String {
        val s = songsService.getSong(song) ?: return "Song Not Found"
        externalResourcesService.searchForLyricsVideo(s, null)
        songsService.songsRepo.saveAndFlush(s)
        return "Completed!"
    }

    @ShellMethod("Search and downloads lyrics version of accept song")
    fun lyricsAccept(song: Int): String {
        val s = externalResourcesService.youtubeEntryRepo.findOne(song) ?: return "Song Not Found"
        externalResourcesService.searchForLyricsVideo(s, null)
        songsService.youtubeRepo.saveAndFlush(s)
        return "Completed!"
    }

    @ShellMethod("List Day Entries")
    fun days(): List<String> = programWeekRepo.findAll().map { toReadableFromStatic(it.relativeStartTime) + " till " + toReadableFromStatic(it.relativeEndTime) }

    @ShellMethod("Volume in Time")
    fun volumeTime(start: String, end: String, volume: Double): String {
        val staticTimeStart = TimeManipulators.parseStaticTimer(start)
        val staticTimeEnd = TimeManipulators.parseStaticTimer(end)
        val vol = VolumeEntry(staticTimeStart = staticTimeStart, staticTimeEnd = staticTimeEnd, volume = volume)
        musicService.volumeRepository.saveAndFlush(vol)
        return "Added time!"
    }

    @ShellMethod("Volume in Time Every Day")
    fun volumeTimeEvery(noWeekends: Boolean, start: String, end: String, volume: Double): String {
        val timerStart = start + "_mon"
        val timerEnd = end + "_mon"
        val staticTimeStart = TimeManipulators.parseStaticTimer(timerStart)
        val staticTimeEnd = TimeManipulators.parseStaticTimer(timerEnd)
        if (staticTimeStart > TimeManipulators.EVERY_DAY_OF_WEEK) {
            return "Invalid time"
        }
        (0..if (noWeekends) 4 else 6)
                .map { VolumeEntry(staticTimeStart = staticTimeStart + TimeManipulators.EVERY_DAY_OF_WEEK * it, staticTimeEnd = staticTimeEnd + TimeManipulators.EVERY_DAY_OF_WEEK * it, volume = volume) }
                .forEach { musicService.volumeRepository.saveAndFlush(it) }
        return "Added time!"
    }

    @ShellMethod("Lock playing")
    fun lockStop(): String {
        SettingsService.stop = !SettingsService.stop
        return "Service status: ${SettingsService.stop}"
    }

    @ShellMethod("Drops the song from playlist")
    fun dropSong(songNumber: Int): String {
        return if (!musicService.removeSong(songNumber)) {
            "ERROR: Song queue position not found!"
        } else {
            "Song successfully removed!"
        }
    }

    @ShellMethod("Inserts song in the place")
    fun insertSong(position: Int, songNumber: Int): String {
        return if (musicService.insertSong(songNumber, position, userRepo.findOne(Constants.CONSOLE_USER))) {
            "Inserted song on position!"
        } else {
            "Error during inserting the song"
        }
    }

    @ShellMethod("Generates random playlist!")
    fun generateRandomPlaylist(count: Int): String {
        (0 until count).forEach { musicService.scheduleSong(musicService.getAndAddRandom().numberId, userRepo.findOne(Constants.CONSOLE_USER)) }
        return "Generated random list with $count elements"
    }

    @ShellMethod("Removes song from db!")
    fun deleteSong(songNumber: Int): String = if (songsService.removeSong(songNumber)) "Successfully Deleted!" else "Error occurred: Song not found"

    @ShellMethod("Removes song from accept list!")
    fun deleteAcceptSong(acceptNumber: Int): String {
        songsService.declineSong(acceptNumber)
        return "Song not accepted!"
    }

    @ShellMethod("Schedule song from playlist")
    fun schedulePlaylist(playlistUrl: String): String {
        musicService.planFromPlaylist(playlistUrl, userRepo.findOne(CONSOLE_USER))
        return "Success!"
    }
}