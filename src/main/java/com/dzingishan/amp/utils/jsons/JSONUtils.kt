package com.dzingishan.amp.utils.jsons

import com.wrapper.spotify.JsonUtil.*
import com.wrapper.spotify.models.*
import net.sf.json.JSONArray
import net.sf.json.JSONException
import net.sf.json.JSONNull
import net.sf.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun createPlaylist(jsonObject: JSONObject): Playlist {
    val returnedPlaylist = Playlist()

    returnedPlaylist.isCollaborative = jsonObject.getBoolean("collaborative")

    if (existsAndNotNull("description", jsonObject)) {
        returnedPlaylist.description = jsonObject.getString("description")
    }
    returnedPlaylist.externalUrls = createExternalUrls(jsonObject.getJSONObject("external_urls"))

    returnedPlaylist.href = jsonObject.getString("href")
    returnedPlaylist.id = jsonObject.getString("id")
    if (existsAndNotNull("images", jsonObject)) {
        returnedPlaylist.images = createImages(jsonObject.getJSONArray("images"))
    }

    returnedPlaylist.name = jsonObject.getString("name")
    returnedPlaylist.owner = createUser(jsonObject.getJSONObject("owner"))
    returnedPlaylist.isPublicAccess = jsonObject.getBoolean("public")

    if (existsAndNotNull("tracks", jsonObject)) {
        returnedPlaylist.tracks = createPlaylistTrackPage(jsonObject.getJSONObject("tracks"))
    }

    returnedPlaylist.type = createSpotifyEntityType(jsonObject.getString("type"))
    returnedPlaylist.uri = jsonObject.getString("uri")
    return returnedPlaylist
}

fun createPlaylistTrackPage(playlistTrackPageJson: JSONObject): Page<PlaylistTrack> {
    val returnedPage = createItemlessPage<PlaylistTrack>(playlistTrackPageJson)
    returnedPage.items = createPlaylistTracks(playlistTrackPageJson.getJSONArray("items"))
    return returnedPage
}

private fun existsAndNotNull(key: String, jsonObject: JSONObject): Boolean = jsonObject.containsKey(key) && JSONNull.getInstance() != jsonObject.get(key)

private fun <T> createItemlessPage(pageJson: JSONObject): Page<T> {
    val page = Page<T>()
    page.href = pageJson.getString("href")
    page.limit = pageJson.getInt("limit")
    if (existsAndNotNull("next", pageJson)) {
        page.next = pageJson.getString("next")
    }
    page.offset = pageJson.getInt("offset")
    if (existsAndNotNull("previous", pageJson)) {
        page.previous = pageJson.getString("previous")
    }
    page.total = pageJson.getInt("total")
    return page
}

private fun createPlaylistTracks(playlistTrackPageJson: JSONArray): List<PlaylistTrack> {
    val returnedPlaylistTracks = playlistTrackPageJson.indices.map { createPlaylistTrack(playlistTrackPageJson.getJSONObject(it)) }
    return returnedPlaylistTracks
}

private fun createPlaylistTrack(playlistTrackJson: JSONObject): PlaylistTrack {
    val returnedPlaylistTrack = PlaylistTrack()
    try {
        returnedPlaylistTrack.addedAt = createDate(playlistTrackJson.getString("added_at"))
    } catch (e: ParseException) {
        returnedPlaylistTrack.addedAt = null
    }

    try {
        returnedPlaylistTrack.addedBy = createUser(playlistTrackJson.getJSONObject("added_by"))
    } catch (e: JSONException) {
        returnedPlaylistTrack.addedBy = null
    }

    returnedPlaylistTrack.track = createTrack(playlistTrackJson.getJSONObject("track"))
    return returnedPlaylistTrack
}

@Throws(ParseException::class)
private fun createDate(dateString: String): Date {
    val formatter = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss")
    formatter.timeZone = TimeZone.getTimeZone("GMT")
    return formatter.parse(dateString)
}

private fun createTrack(trackJson: JSONObject): Track {

    val track = Track()

    //track.album = createSimpleAlbum(trackJson.getJSONObject("album"))
    track.artists = createSimpleArtists(trackJson.getJSONArray("artists"))
    track.availableMarkets = createAvailableMarkets(trackJson.getJSONArray("available_markets"))
    track.discNumber = trackJson.getInt("disc_number")
    track.duration = trackJson.getInt("duration_ms")
    track.isExplicit = trackJson.getBoolean("explicit")
    track.externalIds = createExternalIds(trackJson.getJSONObject("external_ids"))
    track.externalUrls = createExternalUrls(trackJson.getJSONObject("external_urls"))
    track.href = trackJson.getString("href")
    track.id = trackJson.getString("id")
    track.name = trackJson.getString("name")
    track.popularity = trackJson.getInt("popularity")
    track.previewUrl = trackJson.getString("preview_url")
    track.trackNumber = trackJson.getInt("track_number")
    track.type = createSpotifyEntityType(trackJson.getString("type"))
    track.uri = trackJson.getString("uri")

    return track
}

private fun createSimpleArtists(artists: JSONArray): List<SimpleArtist> {
    val returnedArtists = artists.indices.map { createSimpleArtist(artists.getJSONObject(it)) }
    return returnedArtists
}