package com.dzingishan.amp.utils

import com.dzingishan.amp.ProcessingException
import com.dzingishan.amp.responses.JsonResponse
import org.bouncycastle.util.io.pem.PemObject
import org.bouncycastle.util.io.pem.PemWriter
import java.io.StringWriter
import java.security.*
import javax.crypto.Cipher

object CryptographyUtils {

    init {
        createKeys(1024)
    }

    private lateinit var keyGen: KeyPairGenerator
    private lateinit var pair: KeyPair
    private lateinit var privateKey: PrivateKey
    private lateinit var publicKey: PublicKey
    private lateinit var cipher: Cipher
    lateinit var formattedPublicKey: String

    @Throws(NoSuchAlgorithmException::class)
    private fun createKeys(keyLength: Int) {
        cipher = Cipher.getInstance("RSA")
        keyGen = KeyPairGenerator.getInstance("RSA")
        keyGen.initialize(keyLength)
        pair = keyGen.generateKeyPair()
        privateKey = pair.private
        publicKey = pair.public
        val pem = PemObject("RSA_PUBLIC_KEY", publicKey.encoded)
        val w = StringWriter()
        val writer = PemWriter(w)
        writer.writeObject(pem)
        writer.close()
        formattedPublicKey = w.toString()

    }

    fun decrypt(encoded: ByteArray): ByteArray {
        return try {
            cipher.init(Cipher.DECRYPT_MODE, privateKey)
            cipher.doFinal(encoded)
        } catch (e: Exception) {
            e.printStackTrace()
            throw ProcessingException(JsonResponse.PROCESS_ERROR.response, throwable = e)
        }

    }

    fun encrypt(toEncode: ByteArray): ByteArray {
        return try {
            cipher.init(Cipher.ENCRYPT_MODE, publicKey)
            cipher.doFinal(toEncode)
        } catch (e: Exception) {
            e.printStackTrace()
            throw ProcessingException(JsonResponse.PROCESS_ERROR.response, throwable = e)
        }

    }

}
