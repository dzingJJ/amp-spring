package com.dzingishan.amp.components

import com.dzingishan.amp.repositories.PlaylistRepository
import com.dzingishan.amp.repositories.entities.YoutubeEntry
import com.dzingishan.amp.responses.JsonResponse
import com.dzingishan.amp.security.KeyAuthentication
import com.dzingishan.amp.services.ExternalResourcesService
import com.dzingishan.amp.services.MusicService
import com.dzingishan.amp.services.SongService
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/songs")
open class SongsController {

    private var logger = Logger.getLogger(SongsController::class.qualifiedName)

    @Autowired private lateinit var songService: SongService
    @Autowired private lateinit var externalResourcesService: ExternalResourcesService
    @Autowired private lateinit var musicService: MusicService

    @Autowired private lateinit var playlistRepo: PlaylistRepository

    @RequestMapping("reload")
    fun reloadSongs(): JsonResponse<String> {
        logger.trace("/songs/reload start")
        songService.rescanAll()
        return JsonResponse("ok")
    }

    @RequestMapping(value = ["download"], method = [(RequestMethod.POST)])
    fun download(@RequestParam("url") url: String): JsonResponse<String> {
        logger.trace("/songs/download start")
        val auth = SecurityContextHolder.getContext().authentication as KeyAuthentication
        externalResourcesService.addToDownload(url, auth.name)
        return JsonResponse()
    }

    @RequestMapping(value = ["download_list"], method = [(RequestMethod.GET)])
    fun downloadList(): JsonResponse<List<Pair<String, String>>> {
        logger.trace("/songs/download_list start")
        return JsonResponse(data = externalResourcesService.downloadQueue.toList())
    }

    @RequestMapping(value = ["accept_and_tag"], method = [(RequestMethod.POST)])
    fun downloadAndAccept(@RequestParam("id") id: Int, @RequestParam(value = "title", defaultValue = "", required = false) title: String, @RequestParam(value = "author", defaultValue = "", required = false) author: String): JsonResponse<Any> {
        logger.trace("/songs/accept_tag start")
        return if (externalResourcesService.acceptFile(id, title, author)) {
            JsonResponse.OK_RESPONSE
        } else {
            JsonResponse.PROCESS_ERROR
        }
    }

    @RequestMapping(value = ["schedule"], method = [(RequestMethod.POST)])
    fun schedule(@RequestParam("id") songId: Int): JsonResponse<String> {
        logger.trace("/songs/schedule start")
        return if (musicService.scheduleSong(songId, (SecurityContextHolder.getContext().authentication as KeyAuthentication).user)) JsonResponse() else JsonResponse("song_not_found")
    }

    @RequestMapping(value = ["volume"], method = [(RequestMethod.GET)])
    fun getVolume(): JsonResponse<Double> {
        logger.trace("/songs/volume:get start")
        return JsonResponse(data = musicService.volume)
    }

    @RequestMapping(value = ["volume"], method = [(RequestMethod.POST)])
    fun setVolume(@RequestParam("volume") volume: Double): JsonResponse<Any> {
        logger.trace("/songs/volume:post start")
        var vol = volume
        if (vol < 0) {
            vol = 0.1
        }
        musicService.volume = vol
        return JsonResponse.OK_RESPONSE
    }

    @RequestMapping(value = ["clear_playlist"], method = [(RequestMethod.POST)])
    fun clear(@RequestParam(value = "stop", required = false, defaultValue = "false") stop: Boolean): JsonResponse<Any> {
        logger.trace("/songs/clear_playlist start")
        logger.info("Clearing playlist")
        playlistRepo.deleteAll()
        if (stop)
            musicService.fadeOutMusic()
        return JsonResponse.OK_RESPONSE
    }

    @RequestMapping(value = ["skip"], method = [(RequestMethod.POST)])
    fun skip(): JsonResponse<String> {
        logger.trace("/songs/skip start")
        logger.info("Skipping the song")
        musicService.fadeOutMusic()
        return JsonResponse()
    }

    @RequestMapping(value = "accept_list", method = [(RequestMethod.GET)])
    fun getAcceptList(): JsonResponse<List<YoutubeEntry?>?> {
        logger.trace("/songs/accept_list start")
        return JsonResponse(data = externalResourcesService.youtubeEntryRepo.findAll())
    }

    @RequestMapping(value = "decline_accept", method = [(RequestMethod.POST)])
    fun removeFromAcceptList(@RequestParam("acceptId") acceptId: Int): JsonResponse<String> {
        logger.trace("/songs/decline_accept start")
        songService.declineSong(acceptId)
        return JsonResponse()
    }

    @RequestMapping(value = "remove_from_playlist", method = [(RequestMethod.POST)])
    fun removeFromPlaylist(@RequestParam("number") numberId: Int): JsonResponse<String> {
        logger.trace("/songs/remove_from_playlist start")
        return if (musicService.removeSong(numberId)) {
            JsonResponse()
        } else {
            JsonResponse("not_found")
        }
    }

    @RequestMapping(value = "insert_into_playlist", method = [(RequestMethod.POST)])
    fun insertIntoPlaylist(@RequestParam("position") position: Int, @RequestParam("songId") songId: Int): JsonResponse<String> {
        logger.trace("/songs/insert_into_playlist start")
        return if (musicService.insertSong(songId, position, (SecurityContextHolder.getContext().authentication as KeyAuthentication).user)) {
            JsonResponse()
        } else {
            JsonResponse("error")
        }
    }

    @RequestMapping(value = "delete_song", method = [(RequestMethod.POST)])
    fun deleteSong(@RequestParam("songId") songId: Int): JsonResponse<String> {
        logger.trace("/songs/delete_song start")
        return if (musicService.removeSong(songId)) JsonResponse() else JsonResponse("error")
    }

    @RequestMapping(value = "swap_songs", method = [(RequestMethod.POST)])
    fun swapSongs(@RequestParam("song1") song1: Int, @RequestParam("song2") song2: Int): JsonResponse<String> {
        logger.trace("/songs/swap_songs start")
        return if (musicService.swapSongs(song1, song2)) JsonResponse() else JsonResponse("error")
    }

    @RequestMapping("schedule_playlist", method = [RequestMethod.POST])
    fun schedulePlaylist(@RequestParam("url") url: String): JsonResponse<String> {
        logger.trace("/songs.schedule_playlist start")
        return if (musicService.planFromPlaylist(url, (SecurityContextHolder.getContext().authentication as KeyAuthentication).user)) JsonResponse() else JsonResponse("error")
    }

}