package com.dzingishan.amp.components

import com.dzingishan.amp.responses.JsonResponse
import com.dzingishan.amp.security.KeyAuthentication
import com.dzingishan.amp.services.ExternalResourcesService
import com.dzingishan.amp.services.FeaturedPlaylist
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("resources")
open class ExternalResourcesController {

    @Autowired private lateinit var externalResourcesService: ExternalResourcesService

    private var logger = Logger.getLogger(SongsController::class.qualifiedName)

    @RequestMapping(value = "search_youtube", method = [RequestMethod.POST])
    private fun searchYoutube(@RequestParam("keyword") keyword: String): JsonResponse<List<SearchResult>> {
        logger.trace("/resources/search_youtube start")
        return JsonResponse(data = externalResourcesService.searchYoutube(keyword).map { SearchResult(it.first, it.second) })
    }

    @RequestMapping(value = "get_featured", method = [RequestMethod.GET])
    private fun getFeatured(): JsonResponse<List<FeaturedPlaylist>> {
        logger.trace("/resources/search_youtube start")
        return JsonResponse(data = externalResourcesService.getFeaturedPlaylists())
    }

    @RequestMapping(value = "download_spotify_playlist", method = [RequestMethod.POST])
    private fun downloadSpotifyPlaylist(url: String): JsonResponse<String> {
        logger.trace("/resources/search_youtube start")
        return if (externalResourcesService.addToDownload(url, (SecurityContextHolder.getContext().authentication as KeyAuthentication).user.username)) JsonResponse() else JsonResponse("error")
    }
}

data class SearchResult(val title: String, val link: String)