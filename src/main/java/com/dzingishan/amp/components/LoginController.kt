package com.dzingishan.amp.components

import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.entities.UserEntity
import com.dzingishan.amp.responses.JsonResponse
import com.dzingishan.amp.security.AuthenticatorService
import com.dzingishan.amp.utils.CryptographyUtils
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.bcrypt.BCrypt
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
@RequestMapping("login")
open class LoginController {

    private var logger = Logger.getLogger(LoginController::class.qualifiedName)

    @Autowired lateinit var userRepository: UserRepository
    @Autowired lateinit var authService: AuthenticatorService

    /**
     * Endpoint for logging in for new players
     *
     * @param username Username of user
     * @param passKey Password of user
     */
    @RequestMapping(value = "/login", method = [(RequestMethod.POST)])
    fun login(@RequestParam("username") username: String, @RequestParam("passKey") passKey: String, httpServletRequest: HttpServletRequest): JsonResponse<String> {
        logger.trace("/login/login start")
        logger.debug("${httpServletRequest.remoteAddr} is trying to login on account $username")
        val user = userRepository.findOneByUsername(username) ?: return JsonResponse("user_not_found")
        //val password = String(CryptographyUtils.decrypt(passKey.toByteArray(Charsets.UTF_8)))
        val password = passKey //TODO:PUBLIC KEY AUTHORIZATION
        return if (BCrypt.checkpw(password, user.passKey)) {
            logger.info("$username successfully logged in!")
            JsonResponse("ok", additionalData = arrayOf(authService.login(user)))
        } else {
            logger.info("$username: Incorrect password issued!")
            JsonResponse("invalid_password")
        }
    }

    /**
     * Endpoint for checking if user is logged in
     */
    @RequestMapping(value = "/auth_check")
    fun authCheck(httpServletRequest: HttpServletRequest): JsonResponse<Boolean> {
        logger.trace("/login/auth_check start")
        val key = httpServletRequest.getHeader("Auth-Key")
        return JsonResponse("ok", data = authService.isKeyCorrect(key))
    }


    /**
     * Endpoint for creating new user
     *
     * @param user User to create, can be not completed object
     * @return `JsonResponse.OK_RESPONSE` if operation successful, otherwise error
     */
    @RequestMapping(value = "/register", method = [(RequestMethod.POST)])
    fun register(@RequestBody user: UserEntity): JsonResponse<Any> {
        logger.trace("/login/register start")
        if (userRepository.exists(user.username) && userRepository.existsByEmail(user.email))
            return JsonResponse("user_exists")
        logger.info("Creating user ${user.username}")
        userRepository.saveAndFlush(user)
        return JsonResponse.OK_RESPONSE
    }

    /**
     * Endpoint for getting public key for requests
     */
    @RequestMapping(value = "/public_key_request", method = [(RequestMethod.GET)])
    fun requestKey(): JsonResponse<String> {
        logger.trace("/login/public_key_request start")
        return JsonResponse("ok", data = CryptographyUtils.formattedPublicKey)
    }

    /**
     * Endpoint for performing logout of the user
     */
    @RequestMapping(value = "/logout", method = [(RequestMethod.POST)])
    fun logout(http: HttpServletRequest): JsonResponse<String> {
        logger.trace("/login/logout start")
        authService.logout(http.getHeader("Auth-Key"))
        logger.debug("$http. logged out!")
        return JsonResponse()
    }
}