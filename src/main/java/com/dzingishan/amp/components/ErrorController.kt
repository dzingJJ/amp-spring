package com.dzingishan.amp.components

import com.dzingishan.amp.ProcessingException
import com.dzingishan.amp.responses.JsonResponse
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler
import java.lang.Exception
import javax.servlet.http.HttpServletRequest

@ControllerAdvice
open class ErrorController : ResponseEntityExceptionHandler() {

    @ExceptionHandler(ProcessingException::class)
    fun handleError(error: ProcessingException, request: HttpServletRequest): JsonResponse<Any> {
        logger.error("Processing exception in request " + request.contextPath + " with IP " + request.remoteAddr, error)
        return error.response
    }

    /*
    org.apache.tomcat.util.http.fileupload.FileUploadException
     */
    @ExceptionHandler(Exception::class)
    fun handleError(error: Exception, request: HttpServletRequest): JsonResponse<Any> {
        logger.error("Exception caught during processing response: ", error)
        return JsonResponse("error", additionalData = arrayOf(error.message!!), status = HttpStatus.INTERNAL_SERVER_ERROR)
    }

    override fun handleExceptionInternal(ex: Exception, body: Any?, headers: HttpHeaders?, status: HttpStatus?, request: WebRequest?): ResponseEntity<Any> {
        logger.error("Exception caught during processing response: ", ex)
        return JsonResponse<Any>("error", additionalData = arrayOf(ex.message!!)) as ResponseEntity<Any>
    }


}