package com.dzingishan.amp.components

import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.responses.JsonResponse
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("perms")
class PermissionsController {

    var logger = Logger.getLogger(PermissionsController::class.qualifiedName)
    @Autowired private lateinit var userRepo: UserRepository

    @RequestMapping(value = "list", method = [(RequestMethod.GET)])
    fun listPermissions(@RequestParam("username") username: String): JsonResponse<Set<String>> {
        logger.trace("/perms/list start")
        val list = userRepo.findOne(username)?.allPermissions ?: return JsonResponse("invalid_username")
        return JsonResponse("ok", data = list)
    }


}