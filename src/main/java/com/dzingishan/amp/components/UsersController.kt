package com.dzingishan.amp.components

import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.entities.UserEntity
import com.dzingishan.amp.responses.JsonResponse
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

/**
 * Controller of the players
 * Here you can control basic player operations
 * This resource is under (base_ip)/users endpoint
 */
@RestController
@RequestMapping("/users")
class UsersController {

    @Autowired
    private lateinit var userRepository: UserRepository

    var logger = Logger.getLogger(UsersController::class.qualifiedName)

    /**
     * Endpoint for receiving players
     * @param httpServletRequest
     * @return
     */
    @RequestMapping(value = "/get", method = [(RequestMethod.GET)])
    fun getUser(@RequestParam(required = false, value = "username") username: String?): JsonResponse<UserEntity> {
        logger.trace("/users/get start")
        return when {
            username != null -> {
                val user = userRepository.findOneByUsername(username)
                when (user) {
                    null -> JsonResponse("user_not_found")
                    else -> JsonResponse(response = "ok", data = user)
                }
            }
            else -> JsonResponse("user_not_found")
        }
    }


}
