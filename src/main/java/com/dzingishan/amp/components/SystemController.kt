package com.dzingishan.amp.components

import com.dzingishan.amp.responses.JsonResponse
import com.dzingishan.amp.services.SettingsService
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("system")
class SystemController {

    @RequestMapping(value = "settings", method = [(RequestMethod.GET)])
    fun getSettings(@RequestParam("key") key: String): JsonResponse<String> {//TODO
        val r = when (key) {
            "random" -> SettingsService.randomPlaying
            else -> {
                "value_not_found"
            }
        }
        return JsonResponse(additionalData = arrayOf(key, r.toString()))
    }

    @RequestMapping(value = "settings", method = [(RequestMethod.POST)])
    fun setSettings(@RequestParam("key") key: String, @RequestParam("value") value: String): JsonResponse<String> {
        TODO()
        return JsonResponse()
    }


}