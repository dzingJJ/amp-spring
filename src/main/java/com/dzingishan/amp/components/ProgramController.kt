package com.dzingishan.amp.components

import com.dzingishan.amp.kotlin.TimeManipulators
import com.dzingishan.amp.repositories.ProgramDaysRepository
import com.dzingishan.amp.repositories.UserRepository
import com.dzingishan.amp.repositories.entities.ProgramDayEntity
import com.dzingishan.amp.responses.JsonResponse
import com.dzingishan.amp.security.KeyAuthentication
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/program")
@RestController
class ProgramController {

    @Autowired private lateinit var programDayRepo: ProgramDaysRepository
    @Autowired private lateinit var userRepo: UserRepository

    private var logger = Logger.getLogger(ProgramController::class.qualifiedName)

    @RequestMapping(value = "/get_plan", method = [(RequestMethod.GET)])
    fun getDayProgram(): JsonResponse<List<ProgramDayEntity>> {
        logger.trace("/program/get_plan start")
        return JsonResponse(data = programDayRepo.findAll())
    }

    @RequestMapping(value = "/add_to_plan", method = [(RequestMethod.POST)])
    fun putDayProgram(@RequestBody programEntry: ProgramDayEntity): JsonResponse<String> {
        logger.trace("/program/add_to_plan start")
        if (programEntry.creator == "" || programEntry.relativeEndTime < 0 || programEntry.relativeStartTime < 0 || programEntry.relativeStartTime > TimeManipulators.STATIC_TIME_MAX || programEntry.relativeEndTime > TimeManipulators.STATIC_TIME_MAX || programEntry.relativeStartTime <= programEntry.relativeEndTime) {
            return JsonResponse("invalid_data")
        }
        programEntry.entryId = 0
        programEntry.createTime = TimeManipulators.currentTimeWithTimeZoneSeconds()
        programEntry.creator = (SecurityContextHolder.getContext().authentication as KeyAuthentication).user.username
        //TODO: Checking if program is in system (no time overlapping)

        programDayRepo.saveAndFlush(programEntry)
        return JsonResponse()
    }

}