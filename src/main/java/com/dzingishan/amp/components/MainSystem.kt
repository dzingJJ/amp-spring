package com.dzingishan.amp.components


import org.apache.log4j.Logger
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
/**
 * Test component for testing things
 */
class MainSystem {

    var logger = Logger.getLogger(MainSystem::class.qualifiedName)

    @RequestMapping(value = "/403")
    fun error403(): String {
        logger.info("Someone used Error403!")
        return "test"
    }

}
