package com.dzingishan.amp.components

import com.dzingishan.amp.kotlin.TimeManipulators
import com.dzingishan.amp.repositories.PlaylistRepository
import com.dzingishan.amp.repositories.SongRepository
import com.dzingishan.amp.repositories.entities.PlaylistEntry
import com.dzingishan.amp.repositories.entities.SongDetails
import com.dzingishan.amp.repositories.entities.SongEntity
import com.dzingishan.amp.responses.JsonResponse
import com.dzingishan.amp.services.MusicService
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/public")
open class PublicController {

    @Autowired
    private lateinit var musicService: MusicService
    @Autowired private lateinit var playlistRepo: PlaylistRepository
    @Autowired private lateinit var songRepo: SongRepository

    private var logger = Logger.getLogger(PublicController::class.qualifiedName)

    @RequestMapping("/current_song")
    fun getCurrentSong(): JsonResponse<SongDetails> {
        logger.trace("/public/current_song start")
        return if (musicService.currentlyPlaying == null) {
            JsonResponse("currently_no_playing")
        } else {
            JsonResponse("ok", additionalData = arrayOf(musicService.currentlyPlaying!!.scheduler, musicService.secondPassed.toString()), data = musicService.currentlyPlaying!!.song.toSongDetails())
        }
    }

    @RequestMapping("/song")
    fun getSongDetails(@RequestParam(value = "songId") id: Int): JsonResponse<SongEntity> {
        logger.trace("/public/song start")
        val v = songRepo.findOne(id)
        return if (v != null)
            JsonResponse("ok", data = v)
        else
            JsonResponse("song_not_found")
    }


    @RequestMapping("/song_list")
    fun getSongList(): JsonResponse<List<SongDetails>> {
        logger.trace("/public/song_list start")
        val songs = songRepo.findAll().map { it.toSongDetails() }
        return JsonResponse("ok", data = songs)
    }


    @RequestMapping("/playlist")
    fun getPlaylist(): JsonResponse<List<PlaylistEntry>> {
        logger.trace("/public/playlist start")
        return JsonResponse("ok", data = playlistRepo.findAll())
    }

    @RequestMapping("/time")
    fun getTime(): JsonResponse<Long> {
        logger.trace("/public/time start")
        return JsonResponse(data = TimeManipulators.currentTimeWithTimeZoneSeconds())
    }

}