On Debian, you need to install these packages:
- Python and pip (should be installed on debian)
- youtube-dl (sudo pip install --upgrade youtube_dl)
- ffmpeg & ffprobe or avcon (sudo apt-get install libav-tools)
- mp3gain (sudo apt-get install mp3gain
- normalize-audio (sudo apt-get install normalize-audio)
On Windows, everything should be placed in lib folder